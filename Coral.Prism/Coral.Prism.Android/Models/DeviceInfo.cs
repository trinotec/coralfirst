﻿using Coral.Prism.Droid.Models;
using Coral.Prism.Models.Interfaces;
using Xamarin.Forms;

[assembly: Dependency(typeof(DeviceInfo))]
namespace Coral.Prism.Droid.Models
{
    public class DeviceInfo : IDeviceInfo
    {
        public float StatusbarHeight => 0;
    }
}