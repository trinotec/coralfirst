﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Coral.Prism.Helper
{
    
    static class HelpFormat
    {        
        public static string FormatoFecha(TimeSpan tiempo)
        {
            string cadena;
            if (tiempo.Hours > 12)
            {
                cadena = (tiempo.Hours - 12).ToString("00") + ":" + tiempo.Minutes.ToString("00") + " pm";
            }
            else
            {
                cadena = tiempo.Hours.ToString("00") + ":" + tiempo.Minutes.ToString("00") + " am";
            }
            return cadena;
        }

        public static string MesString(int Mes)
        {
            string MesString = "";
            switch (Mes)
            {
                case 1:
                    MesString = "Ene";
                    break;
                case 2:
                    MesString = "Feb";
                    break;
                case 3:
                    MesString = "Mar";
                    break;
                case 4:
                    MesString = "Abr";
                    break;
                case 5:
                    MesString = "May";
                    break;
                case 6:
                    MesString = "Jun";
                    break;
                case 7:
                    MesString = "Jul";
                    break;
                case 8:
                    MesString = "Ago";
                    break;
                case 9:
                    MesString = "Sep";
                    break;
                case 10:
                    MesString = "Oct";
                    break;
                case 11:
                    MesString = "Nov";
                    break;
                case 12:
                    MesString = "Dic";
                    break;

            }
            return MesString;
        }


        public static string FormatoFechaDatetime(DateTime tiempo)
        {
            string cadena;
            if (tiempo.Hour > 12)
            {
                cadena = (tiempo.Hour - 12).ToString("00") + ":" + tiempo.Minute.ToString("00") + " pm";
            }
            else
            {
                cadena = tiempo.Hour.ToString("00") + ":" + tiempo.Minute.ToString("00") + " am";
            }
            return cadena;
        }
    }
}
