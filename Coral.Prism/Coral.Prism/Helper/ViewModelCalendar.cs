﻿using Coral.Prism.ViewModels;
using Syncfusion.SfCalendar.XForms;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Coral.Prism.Helper
{
    public class ViewModelCalendar
    {

        public CalendarEventCollection CalendarInlineEvents { get; set; } = new CalendarEventCollection();

        public ViewModelCalendar()
        {

            // Create events 
            /*
            CalendarInlineEvent event1 = new CalendarInlineEvent()
            {
                StartTime = DateTime.Today.AddHours(9),
                EndTime = DateTime.Today.AddHours(10),
                Subject = "Meeting",
                Color = Color.Green
            };

            CalendarInlineEvent event2 = new CalendarInlineEvent()
            {
                StartTime = DateTime.Today.AddHours(11),
                EndTime = DateTime.Today.AddHours(12),
                Subject = "Planning",
                Color = Color.Fuchsia
            };

            CalendarInlineEvent event3 = new CalendarInlineEvent()
            {
                StartTime = DateTime.Today.AddDays(4).AddHours(11),
                EndTime = DateTime.Today.AddDays(4).AddHours(12),
                Subject = "Planning",
                Color = Color.Fuchsia
            }; */

            /*
            var EFVM = EventoFechaPageViewModel.GetInstance();
           

            if(EFVM != null)
            {
                foreach (var Evento in EFVM.Eventos)
                {
                    CalendarInlineEvent eventoTemp = new CalendarInlineEvent()
                    {
                        StartTime = Evento.HoraInicio,
                        EndTime = Evento.HoraInicio,
                        Subject = Evento.NombreEvento,
                        Color = Color.Fuchsia
                    };
                    CalendarInlineEvents.Add(eventoTemp);
                }
            } */

            // Add events into a CalendarInlineEvents collection
            /*
            CalendarInlineEvents.Add(event1);
            CalendarInlineEvents.Add(event2);
            CalendarInlineEvents.Add(event3);]*/
        }
    }
}
