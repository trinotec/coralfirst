﻿using Plugin.Settings;
using Plugin.Settings.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Coral.Prism.Helper
{
    public static class Settings
    {

        private const string _user = "USER";
        private const string _isLogin = "Login";
        private const string _news = "news";
        private const string _rol = "rol";

        //private const string _token = "TOKEN";
        private static readonly string _stringDefault = string.Empty;
        private static readonly bool _boolDefault = false;
        private static readonly int _intDefault = 2;


        private static ISettings AppSettings => CrossSettings.Current;

        public static string User
        {
            get => AppSettings.GetValueOrDefault(_user, _stringDefault);
            set => AppSettings.AddOrUpdateValue(_user, value);
        }

        public static bool IsLogin
        {
            get => AppSettings.GetValueOrDefault(_isLogin, _boolDefault);
            set => AppSettings.AddOrUpdateValue(_isLogin, value);
        }

        public static string News
        {
            get => AppSettings.GetValueOrDefault(_news, _stringDefault);
            set => AppSettings.AddOrUpdateValue(_news, value);
        }

        public static int Rol
        {
            get => AppSettings.GetValueOrDefault(_rol, _intDefault);
            set => AppSettings.AddOrUpdateValue(_rol, value);
        }

        //public static string Token
        //{
        //    get => AppSettings.GetValueOrDefault(_token, _stringDefault);
        //    set => AppSettings.AddOrUpdateValue(_token, value);
        //}
    }
}
