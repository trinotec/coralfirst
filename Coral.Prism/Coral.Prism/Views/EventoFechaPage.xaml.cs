﻿using Coral.Prism.ViewModels;
using Unity.Injection;
using Xamarin.Forms;

namespace Coral.Prism.Views
{
    public partial class EventoFechaPage : ContentPage
    {
        public EventoFechaPage()
        {
            InitializeComponent();            
        }

     

        private  void calendar_SelectionChanged(object sender, Syncfusion.SfCalendar.XForms.SelectionChangedEventArgs e)
        {
            var OPVM = ((EventoFechaPageViewModel)this.BindingContext);            
            OPVM.MostrarEventosDias();
        }

        private void calendar_MonthChanged(object sender, Syncfusion.SfCalendar.XForms.MonthChangedEventArgs e)
        {
            //var OPVM = ((EventoFechaPageViewModel)this.BindingContext);
            //Syncfusion.SfCalendar.XForms.SfCalendar Calendario = (Syncfusion.SfCalendar.XForms.SfCalendar)sender;
            //OPVM.MostrarEventosMes(Calendario.MoveToDate.Month);
        }
    }
}
