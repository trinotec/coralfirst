﻿using Coral.Prism.Models;
using Coral.Prism.ViewModels;
using Prism.Navigation;
using System;
using Xamarin.Forms;

namespace Coral.Prism.Views
{
    public partial class EventosDatosPage : ContentPage
    {
        
        public EventosDatosPage()
        {
            InitializeComponent();            
        }
        private void TapGestureRecognizer_Tapped(object sender, System.EventArgs e)
        {
            var OPVM = ((EventosDatosPageViewModel)this.BindingContext);
            OPVM.MosTrarEstablecerHorarios();         
        }

        private void Button_Clicked(object sender, System.EventArgs e)
        {
            var OPVM = ((EventosDatosPageViewModel)this.BindingContext);
            OPVM.EstablecerHorarios();
            ETHoras.Text = OPVM.HorariosEvento;
        }       
    }
}
