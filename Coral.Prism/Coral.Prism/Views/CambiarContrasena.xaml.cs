﻿using Xamarin.Forms;

namespace Coral.Prism.Views
{
    public partial class CambiarContrasena : ContentPage
    {
        public CambiarContrasena()
        {
            InitializeComponent();

            TxtPass.TextChanged += (object sender, TextChangedEventArgs e) =>
            { 
                ValidacionButton();
            };

            TxtRepPass.TextChanged += (object sender, TextChangedEventArgs e) =>
            {
                ValidacionButton();
            };

        }


        public void ValidacionButton()
        {
            
            if (!string.IsNullOrEmpty(TxtPass.Text) && !string.IsNullOrEmpty(TxtRepPass.Text))
            {                
                btnGuardar.BackgroundColor = Color.FromHex("F70041");
                btnGuardar.IsEnabled = true;
            }
            else
            {
                btnGuardar.BackgroundColor = Color.FromHex("cec1c1");
                btnGuardar.IsEnabled = false;
            }  
        }


    }
}
