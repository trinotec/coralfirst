﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Coral.Prism.Views
{
    public partial class NonModalSubPage : BasePage
    {
        public NonModalSubPage()
        {
            InitializeComponent();
        }
    }
}