﻿using Coral.Prism.ViewModels;
using Xamarin.Forms;

namespace Coral.Prism.Views
{
    public partial class OpinionPage : BasePage
    {
         
        public OpinionPageViewModel OPVM;

        public OpinionPage()
        {
            OPVM = ((OpinionPageViewModel)this.BindingContext);
            InitializeComponent();
            
        }

        private void next_Clicked(object sender, System.EventArgs e)
        {

            int itemcount = OPVM.Encuestas.Detalle.Count;
            var encuestaActual = OPVM.Encuestas.Detalle[CarouselEncuesta.Position];            
            if (! string.IsNullOrEmpty(encuestaActual.TextoRespuesta))
            {
                if (CarouselEncuesta.Position + 1 != itemcount)
                {
                    CarouselEncuesta.Position = CarouselEncuesta.Position + 1;
                }
                else
                {
                    //Oculto y desoculto
                    FinalEncuesta();
                }
            }
        }

        private void listView_BindingContextChanged(object sender, System.EventArgs e)
        {
            int itemcount = OPVM.Encuestas.Detalle.Count;

            if (CarouselEncuesta.Position + 1 != itemcount)
            {
                CarouselEncuesta.Position = CarouselEncuesta.Position + 1;
            }
            else
            {
                //Oculto y desoculto
                FinalEncuesta();
            }  
        }

        private void FinalEncuesta()
        {
            StlyCarousel.IsVisible = false;
            StlyFinalEnc.IsVisible = true;
            OPVM.EnviarEncuestas();            
        }

    }
}
