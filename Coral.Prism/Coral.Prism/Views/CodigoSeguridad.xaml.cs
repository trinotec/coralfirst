﻿using Xamarin.Forms;

namespace Coral.Prism.Views
{
    public partial class CodigoSeguridad : ContentPage
    {
        public CodigoSeguridad()
        {
            InitializeComponent();
            
            TxtCode1.TextChanged += (object sender, TextChangedEventArgs e) =>
            {
                if (!string.IsNullOrEmpty(e.NewTextValue.Trim()))
                {
                    TxtCode2.Focus();                    
                }
                ValidacionButton();


            };

            TxtCode2.TextChanged += (object sender, TextChangedEventArgs e) => {
                if (!string.IsNullOrEmpty(e.NewTextValue.Trim()))
                {
                    TxtCode3.Focus();                    
                }
                ValidacionButton();

            };

            TxtCode3.TextChanged += (object sender, TextChangedEventArgs e) => {
                if (!string.IsNullOrEmpty(e.NewTextValue.Trim()))
                {
                    TxtCode4.Focus();                    
                }
                ValidacionButton();
            };

            TxtCode4.TextChanged += (object sender, TextChangedEventArgs e) => {
                if (!string.IsNullOrEmpty(e.NewTextValue.Trim()))
                {
                    TxtCode5.Focus();                    
                }
                ValidacionButton();
            };

            TxtCode5.TextChanged += (object sender, TextChangedEventArgs e) => {
                ValidacionButton();
            };

        }

        public void ValidacionButton()
        {
            string codigo = TxtCode1.Text + TxtCode2.Text + TxtCode3.Text + TxtCode4.Text + TxtCode5.Text;

            if (!string.IsNullOrEmpty(codigo) && codigo.Length >= 5)
            {
                //TxtCode2.Focus();
                btnSiguiente.BackgroundColor = Color.FromHex("F70041");
                btnSiguiente.IsEnabled = true;
            }
            else
            {
                btnSiguiente.BackgroundColor = Color.FromHex("cec1c1");
                btnSiguiente.IsEnabled = false;
            }


        }


    }
}
