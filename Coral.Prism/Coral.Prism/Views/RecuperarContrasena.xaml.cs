﻿using Xamarin.Forms;

namespace Coral.Prism.Views
{
    public partial class RecuperarContrasena : ContentPage
    {
        public RecuperarContrasena()
        {
            InitializeComponent();
                TxtEmail.TextChanged += (object sender, TextChangedEventArgs e) =>
                {
                    if (!string.IsNullOrEmpty(e.NewTextValue.Trim()))
                    {
                        //TxtCode2.Focus();
                        btnSiguiente.BackgroundColor = Color.FromHex("F70041"); 
                    }
                    else
                    {
                        btnSiguiente.BackgroundColor = Color.FromHex("cec1c1");
                    }   
                };
        }
    }
}
