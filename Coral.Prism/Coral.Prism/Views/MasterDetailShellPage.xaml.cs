﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Coral.Prism.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MasterDetailShellPage : MasterDetailPage
    {
        public MasterDetailShellPage()
        {
            InitializeComponent();
        }
    }
}