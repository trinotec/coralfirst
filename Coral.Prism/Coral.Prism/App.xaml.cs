﻿using Prism;
using Prism.Ioc;
using Coral.Prism.ViewModels;
using Coral.Prism.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Coral.Prism.Services.Interfaces;
using Coral.Prism.Services;
using Coral.Prism.Helper;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace Coral.Prism
{
    public partial class App
    {
        /* 
         * The Xamarin Forms XAML Previewer in Visual Studio uses System.Activator.CreateInstance.
         * This imposes a limitation in which the App class must have a default constructor. 
         * App(IPlatformInitializer initializer = null) cannot be handled by the Activator.
         */
        public App() : this(null) { }

        public App(IPlatformInitializer initializer) : base(initializer) { }

        protected override async void OnInitialized()
        {
            Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense("MTg3NzI1QDMxMzcyZTM0MmUzMGwxYkJGOW1KcEdkZ0hoQ3BLZUUwZC9SblRZQXJKSjRVdWppWm9NTy8rZjQ9");
            InitializeComponent();
            Settings.News = string.Empty;
            if (Settings.IsLogin == true)
            {
                var result = await NavigationService.NavigateAsync("/MasterDetailShellPage/NavigationPage/MainPage");
            }
            else
            {
                await NavigationService.NavigateAsync("/LoginPage");
                //await NavigationService.NavigateAsync("/EventoFechaPage");
            }



            //var result = await NavigationService.NavigateAsync("/NavigationPage/LoginPage");
            //var result = await NavigationService.NavigateAsync("MasterDetailShellPage/NavigationPage/MainPage");
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterSingleton<IPopupService, PopupService>();

            containerRegistry.RegisterForNavigation<NavigationPage>();
            containerRegistry.RegisterForNavigation<MasterDetailShellPage, MasterDetailShellPageViewModel>();
            containerRegistry.RegisterForNavigation<MenuPage>();
            containerRegistry.RegisterForNavigation<BasePage>();
            containerRegistry.RegisterForNavigation<MainPage, MainPageViewModel>();
            containerRegistry.RegisterForNavigation<NonModalSubPage, NonModalSubViewModel>();
            containerRegistry.RegisterForNavigation<ModalSubPage, ModalSubViewModel>();

            containerRegistry.RegisterForNavigation<OpinionPage, OpinionPageViewModel>();
            containerRegistry.Register<IApiService, ApiService>();
            containerRegistry.RegisterForNavigation<NoticiasPage, NoticiasPageViewModel>();
            containerRegistry.RegisterForNavigation<NoticiaDetallePage, NoticiaDetallePageViewModel>();
            containerRegistry.RegisterForNavigation<LoginPage, LoginPageViewModel>();
            containerRegistry.RegisterForNavigation<RecuperarContrasena, RecuperarContrasenaViewModel>();
            containerRegistry.RegisterForNavigation<CodigoSeguridad, CodigoSeguridadViewModel>();
            containerRegistry.RegisterForNavigation<CambiarContrasena, CambiarContrasenaViewModel>();
            containerRegistry.RegisterForNavigation<ProfilePage, ProfilePageViewModel>();
            containerRegistry.RegisterForNavigation<EventosPage, EventosPageViewModel>();
            containerRegistry.RegisterForNavigation<EventoFechaPage, EventoFechaPageViewModel>();            
            containerRegistry.RegisterForNavigation<EventosDatosPage, EventosDatosPageViewModel>();
        }
    }
}
