﻿using Coral.Prism.Models;
using Coral.Prism.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Coral.Prism.Controls
{
    class PersonDataTemplateSelector : DataTemplateSelector
    {
        public DataTemplate PreguntaAbierta { get; set; }
        public DataTemplate PreguntaCerrada { get; set; }

        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            return ((DetallaViewModel)item).DetallePreguntaAbierta == true ? PreguntaAbierta : PreguntaCerrada;
        }
    }
}
