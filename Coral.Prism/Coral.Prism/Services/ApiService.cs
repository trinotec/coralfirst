﻿using Coral.Prism.Models;
using Coral.Prism.Services.Interfaces;
using Coral.Prism.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Coral.Prism.Services
{
    public class ApiService : IApiService
    {
        public async Task<Response<MenuComidaResponse>> GetMenuByCategoria(string urlBase, string servicePrefix, string controller)
        {
            try
            {

                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };

                var url = $"{urlBase}/{servicePrefix}{controller}";
                UriBuilder builder = new UriBuilder(url);

                var response = await client.GetAsync(builder.Uri);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response<MenuComidaResponse>
                    {
                        IsSuccess = false,
                        Message = JsonConvert.DeserializeObject<string>(result)
                    };
                }

                var _data = JsonConvert.DeserializeObject<MenuComidaResponse>(result);
                return new Response<MenuComidaResponse>
                {
                    IsSuccess = true,
                    Result = _data
                };
            }
            catch (Exception ex)
            {
                string msgError;
                switch (ex.Message)
                {
                    case "No such host is known":
                        msgError = "No se encontro el servidor";
                        break;
                    default:
                        msgError = "No se pudo conectar al servidor";
                        break;
                }
                return new Response<MenuComidaResponse>
                {
                    IsSuccess = false,
                    Message = msgError
                };
            }
        }
        public async Task<Response<object>> GetListNoticias(string urlBase, string servicePrefix, string controller)
        {
            try
            {

                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };

                var url = $"{urlBase}/{servicePrefix}{controller}";
                //UriBuilder builder = new UriBuilder(url);

                var response = await client.GetAsync(url);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response<object>
                    {
                        IsSuccess = false,
                        Message = JsonConvert.DeserializeObject<string>(result)
                    };
                }

                var noticias = JsonConvert.DeserializeObject<List<Noticia>>(result);
                return new Response<object>
                {
                    IsSuccess = true,
                    Result = noticias
                };
            }
            catch (Exception ex)
            {
                string msgError;
                switch (ex.Message)
                {
                    case "No such host is known":
                        msgError = "No se encontro el servidor";
                        break;
                    default:
                        msgError = "No se pudo conectar al servidor";
                        break;
                }
                return new Response<object>
                {
                    IsSuccess = false,
                    Message = msgError
                };
            }
        }

        public async Task<Response<LoginResponse>> GetLoginAsync(string urlBase, string servicePrefix, string controller, LoginRequest request)
        {
            try
            {

                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };

                var url = $"{urlBase}/{servicePrefix}{controller}";
                UriBuilder builder = new UriBuilder(url);
                builder.Query = string.Format("codigo={0}&pw={1}", request.codigo, request.pw);

                var response = await client.GetAsync(builder.Uri);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response<LoginResponse>
                    {
                        IsSuccess = false,
                        Message = JsonConvert.DeserializeObject<string>(result)
                    };
                }

                var token = JsonConvert.DeserializeObject<LoginResponse>(result);
                return new Response<LoginResponse>
                {
                    IsSuccess = true,
                    Result = token
                };
            }
            catch (Exception ex)
            {
                string msgError;
                switch (ex.Message)
                {
                    case "No such host is known":
                        msgError = "No se encontro el servidor";
                        break;
                    default:
                        msgError = "No se pudo conectar al servidor";
                        break;
                }
                return new Response<LoginResponse>
                {
                    IsSuccess = false,
                    Message = msgError
                };
            }
        }

        public async Task<Response<CodeRecuperacionResponse>> GetCodeRecuperacionAsync(string urlBase, string servicePrefix, string controller, CodeRecuperacionRequest request)
        {
            try
            {

                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };

                var url = $"{urlBase}/{servicePrefix}{controller}";
                UriBuilder builder = new UriBuilder(url);
                builder.Query = string.Format("codigo={0}", request.codigo);

                var response = await client.GetAsync(builder.Uri);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response<CodeRecuperacionResponse>
                    {
                        IsSuccess = false,
                        Message = JsonConvert.DeserializeObject<string>(result)
                    };
                }

                var token = JsonConvert.DeserializeObject<CodeRecuperacionResponse>(result);
                return new Response<CodeRecuperacionResponse>
                {
                    IsSuccess = true,
                    Result = token
                };
            }
            catch (Exception ex)
            {
                string msgError;
                switch (ex.Message)
                {
                    case "No such host is known":
                        msgError = "No se encontro el servidor";
                        break;
                    default:
                        msgError = "No se pudo conectar al servidor";
                        break;
                }
                return new Response<CodeRecuperacionResponse>
                {
                    IsSuccess = false,
                    Message = msgError
                };
            }
        }

        public async Task<Response<UserResponse>> UpdateProfile(string urlBase, string servicePrefix, string controller, UserRequest request)
        {
            try
            {

                var requestString = JsonConvert.SerializeObject(request);
                var content = new StringContent(requestString, Encoding.UTF8, "application/json");

                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };
                client.Timeout = new TimeSpan(0, 5, 0);

                var url = $"{urlBase}/{servicePrefix}{controller}";

                var response = await client.PutAsync(url, content);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response<UserResponse>
                    {
                        IsSuccess = false,
                        Message = JsonConvert.DeserializeObject<string>(result),
                    };
                }

                var list = JsonConvert.DeserializeObject<UserResponse>(result);
                return new Response<UserResponse>
                {
                    IsSuccess = true,
                    Result = list
                };
            }
            catch (Exception ex)
            {
                string msgError;
                switch (ex.Message)
                {
                    case "No such host is known":
                        msgError = "No se encontro el servidor";
                        break;
                    default:
                        msgError = "No se pudo conectar al servidor";
                        break;
                }
                return new Response<UserResponse>
                {
                    IsSuccess = false,
                    Message = msgError
                };
            }
        }

        public async Task<Response<List<Encuesta>>> GetEncuesta(string urlBase, string servicePrefix, string controller)
        {
            try
            {

                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };

                var url = $"{urlBase}/{servicePrefix}{controller}";                

                var response = await client.GetAsync(url);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response<List<Encuesta>>
                    {
                        IsSuccess = false,
                        Message = JsonConvert.DeserializeObject<string>(result)
                    };
                }

                var Encuestas = JsonConvert.DeserializeObject<List<Encuesta>>(result);
                return new Response<List<Encuesta>>
                {
                    IsSuccess = true,
                    Result = Encuestas
                };
            }
            catch (Exception ex)
            {
                string msgError;
                switch (ex.Message)
                {
                    case "No such host is known":
                        msgError = "No se encontro el servidor";
                        break;
                    default:
                        msgError = "No se pudo conectar al servidor";
                        break;
                }
                return new Response<List<Encuesta>>
                {
                    IsSuccess = false,
                    Message = msgError
                };
            }
        }

        public async Task<Response<EncuestaResponse>> SendResultEncuesta(string urlBase, string servicePrefix, string controller, List<EncuestaRequest> request)
        {
            try
            {

                var requestString = JsonConvert.SerializeObject(request);
                var content = new StringContent(requestString, Encoding.UTF8, "application/json");

                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };

                var url = $"{urlBase}/{servicePrefix}{controller}";

                var response = await client.PostAsync(url, content);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response<EncuestaResponse>
                    {
                        IsSuccess = false,
                        Message = JsonConvert.DeserializeObject<string>(result),
                    };
                }

                var list = JsonConvert.DeserializeObject<EncuestaResponse>(result);
                return new Response<EncuestaResponse>
                {
                    IsSuccess = true,
                    Result = list
                };
            }
            catch (Exception ex)
            {
                string msgError;
                switch (ex.Message)
                {
                    case "No such host is known":
                        msgError = "No se encontro el servidor";
                        break;
                    default:
                        msgError = "No se pudo conectar al servidor";
                        break;
                }
                return new Response<EncuestaResponse>
                {
                    IsSuccess = false,
                    Message = msgError
                };
            }
        }

        /*
        public async Task<Response<CambiarContrasenaResponse>> UpdateContrasena(string urlBase, string servicePrefix, string controller, CambiarContrasenaRequest request)
        {
            try
            {

                var requestString = JsonConvert.SerializeObject(request);
                var content = new StringContent(requestString, Encoding.UTF8, "application/json");

                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };

                var url = $"{urlBase}/{servicePrefix}{controller}";

                var response = await client.PutAsync(url, content);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response<CambiarContrasenaResponse>
                    {
                        IsSuccess = false,
                        Message = JsonConvert.DeserializeObject<string>(result),
                    };
                }

                var list = JsonConvert.DeserializeObject<CambiarContrasenaResponse>(result);
                return new Response<CambiarContrasenaResponse>
                {
                    IsSuccess = true,
                    Result = list
                };
            }
            catch (Exception ex)
            {
                string msgError;
                switch (ex.Message)
                {
                    case "No such host is known":
                        msgError = "No se encontro el servidor";
                        break;
                    default:
                        msgError = "No se pudo conectar al servidor";
                        break;
                }
                return new Response<CambiarContrasenaResponse>
                {
                    IsSuccess = false,
                    Message = msgError
                };
            }
        }

        */
        public async Task<Response<string>> SendEvento(string urlBase, string servicePrefix, string controller, EventoRequest request)
        {
            try
            {

                var requestString = JsonConvert.SerializeObject(request);
                var content = new StringContent(requestString, Encoding.UTF8, "application/json");

                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };

                var url = $"{urlBase}/{servicePrefix}{controller}";

                var response = await client.PostAsync(url, content);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response<string>
                    {
                        IsSuccess = false,
                        Message = JsonConvert.DeserializeObject<string>(result),
                    };
                }

                //var list = JsonConvert.DeserializeObject<EncuestaResponse>(result);
                return new Response<string>
                {
                    IsSuccess = true,
                    Result = "Ok"
                };


            }
            catch (Exception ex)
            {
                string msgError;
                switch (ex.Message)
                {
                    case "No such host is known":
                        msgError = "No se encontro el servidor";
                        break;
                    default:
                        msgError = "No se pudo conectar al servidor";
                        break;
                }
                return new Response<string>
                {
                    IsSuccess = false,
                    Message = msgError
                };
            }
        }



        public async Task<Response<List<Evento>>> GetListEventos(string urlBase, string servicePrefix, string controller, string CodUser)
        {
            try
            {

                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };

                var url = $"{urlBase}/{servicePrefix}{controller}?CodCliente={CodUser}";
                //UriBuilder builder = new UriBuilder(url);

                var response = await client.GetAsync(url);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response<List<Evento>>
                    {
                        IsSuccess = false,
                        Message = JsonConvert.DeserializeObject<string>(result)
                    };
                }

                var list = JsonConvert.DeserializeObject<List<Evento>>(result);
                return new Response<List<Evento>>
                {
                    IsSuccess = true,
                    Result = list
                };
            }
            catch (Exception ex)
            {
                string msgError;
                switch (ex.Message)
                {
                    case "No such host is known":
                        msgError = "No se encontro el servidor";
                        break;
                    default:
                        msgError = "No se pudo conectar al servidor";
                        break;
                }
                return new Response<List<Evento>>
                {
                    IsSuccess = false,
                    Message = msgError
                };
            }
        }

        public async Task<Response<string>> EditEvento(string urlBase, string servicePrefix, string controller, EventoRequest request)
        {
            try
            {

                var requestString = JsonConvert.SerializeObject(request);
                var content = new StringContent(requestString, Encoding.UTF8, "application/json");

                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };

                var url = $"{urlBase}/{servicePrefix}{controller}";

                var response = await client.PutAsync(url, content);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response<string>
                    {
                        IsSuccess = false,
                        Message = JsonConvert.DeserializeObject<string>(result),
                    };
                }

                //var list = JsonConvert.DeserializeObject<EncuestaResponse>(result);
                return new Response<string>
                {
                    IsSuccess = true,
                    Result = "Ok"
                };


            }
            catch (Exception ex)
            {
                string msgError;
                switch (ex.Message)
                {
                    case "No such host is known":
                        msgError = "No se encontro el servidor";
                        break;
                    default:
                        msgError = "No se pudo conectar al servidor";
                        break;
                }
                return new Response<string>
                {
                    IsSuccess = false,
                    Message = msgError
                };
            }
        }

        public async Task<Response<ResponseCambiarContraseña>> UpdateContrasena(string urlBase, string servicePrefix, string controller, CambiarContrasenaRequest request, string codigo)
        {
            try
            {

                var requestString = JsonConvert.SerializeObject(request);
                var content = new StringContent(requestString, Encoding.UTF8, "application/json");

                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase),                     
                };
                var url = $"{urlBase}/{servicePrefix}{controller}?codigo={codigo}";

                //Nota: para este caso se utlizo el SendAsync por que es un api del tipo get con un body json, en cualquier otro caso usar. GetAsync
                var requestHttp = new HttpRequestMessage
                {
                    Method = HttpMethod.Get,
                    RequestUri = new Uri(url),
                    Content = new StringContent(requestString, Encoding.UTF8, "application/json"),
                };
                var response = await client.SendAsync(requestHttp).ConfigureAwait(false);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response<ResponseCambiarContraseña>
                    {
                        IsSuccess = false,
                        Message = JsonConvert.DeserializeObject<string>(result)
                    };
                }


                var list = JsonConvert.DeserializeObject<ResponseCambiarContraseña>(result);
                return new Response<ResponseCambiarContraseña>
                {
                    IsSuccess = true,
                    Result = list
                };
            }
            catch (Exception ex)
            {
                string msgError;
                switch (ex.Message)
                {
                    case "No such host is known":
                        msgError = "No se encontro el servidor";
                        break;
                    default:
                        msgError = "No se pudo conectar al servidor";
                        break;
                }
                return new Response<ResponseCambiarContraseña>
                {
                    IsSuccess = false,
                    Message = msgError
                };
            }
        }


    }
}
