﻿using Coral.Prism.Models;
using Coral.Prism.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Coral.Prism.Services.Interfaces
{
    public interface IApiService
    {
        Task<Response<MenuComidaResponse>> GetMenuByCategoria(
           string urlBase,
           string servicePrefix,
           string controller
           );

        Task<Response<object>> GetListNoticias(
            string urlBase,
            string servicePrefix,
            string controller
         );

        Task<Response<LoginResponse>> GetLoginAsync(
            string urlBase,
            string servicePrefix,
            string controller,
            LoginRequest request
            );

       Task<Response<CodeRecuperacionResponse>> GetCodeRecuperacionAsync(
           string urlBase,
           string servicePrefix,
           string controller,
           CodeRecuperacionRequest request
        );

        Task<Response<UserResponse>> UpdateProfile(
           string urlBase,
           string servicePrefix,
           string controller,
           UserRequest request
        );

        Task<Response<List<Encuesta>>> GetEncuesta(
            string urlBase,
            string servicePrefix,
            string controller            
         );


        Task<Response<EncuestaResponse>> SendResultEncuesta(
            string urlBase,
            string servicePrefix,
            string controller,
            List<EncuestaRequest> request
         );

        /*
        Task<Response<CambiarContrasenaResponse>> UpdateContrasena(
            string urlBase,
            string servicePrefix,
            string controller,
            CambiarContrasenaRequest request
         ); */

        Task<Response<String>> SendEvento(
            string urlBase,
            string servicePrefix,
            string controller,
            EventoRequest request
         );

        Task<Response<List<Evento>>> GetListEventos(
            string urlBase,
            string servicePrefix,
            string controller,
            string CodUser
         );

        Task<Response<String>> EditEvento(
            string urlBase,
            string servicePrefix,
            string controller,
            EventoRequest request
         );

        Task<Response<ResponseCambiarContraseña>> UpdateContrasena(
            string urlBase,
            string servicePrefix,
            string controller,
            CambiarContrasenaRequest request,
            string codigo
         );

    }
}
