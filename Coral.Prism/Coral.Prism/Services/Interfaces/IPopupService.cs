﻿using Coral.Prism.Controls;
using Prism.Commands;

namespace Coral.Prism.Services.Interfaces
{
    public interface IPopupService
    {
        void DisplayPopup(string title, string content, DelegateCommand<PopupResultEventArgs> command = null);
        bool Dismiss();
    }
}
