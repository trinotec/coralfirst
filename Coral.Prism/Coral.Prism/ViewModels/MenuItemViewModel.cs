﻿using Coral.Prism.Models;
using Prism.Commands;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Coral.Prism.ViewModels
{
    public class MenuItemViewModel : Menu
    {

        private readonly INavigationService _navigationService;
        private DelegateCommand _selectMenuCommand;

        public MenuItemViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;
        }

        public DelegateCommand SelectMenuCommand => _selectMenuCommand ?? (_selectMenuCommand = new DelegateCommand(SelectMenu));

        private void SelectMenu()
        {
            var page = this.Index;
            var mainpage = MainPageViewModel.GetInstance();
            mainpage.UpdateTabMenu(page);

            //if (PageName.Equals("NoticiasPage"))
            //{
            //    var nav = _navigationService.GetNavigationUriPath();
            //    await _navigationService.NavigateAsync("NavigationPage/MainPage/NoticiasPage");
            //}
            //else
            //{                
            //    var p = new NavigationParameters();
            //    p.Add("Index", this.Index);
            //    await _navigationService.NavigateAsync($"/MasterDetailShellPage/NavigationPage/{PageName}", p);
            //}
         
        }
    }
}
