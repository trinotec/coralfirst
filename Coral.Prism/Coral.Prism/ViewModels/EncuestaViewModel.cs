﻿using Coral.Prism.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Coral.Prism.ViewModels
{
    public class EncuestaViewModel 
    {
        public int IdEncuesta { get; set; }
        public string Titulo { get; set; }
        public string Descripcion { get; set; }
        public string CodCentroCosto { get; set; }
        public List<DetallaViewModel> Detalle { get; set; }

        

        public void EnumeracionPreguntas()
        {
            int conta = 0;
            foreach(var item in Detalle)
            {
                conta++;
                item.PiePregunta = conta + " de " + Detalle.Count; 

            }
        }

    }
}
