﻿using Coral.Prism.Models;
using Prism.Commands;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Coral.Prism.ViewModels
{
   public class EventoVM : Evento
    {
        private readonly INavigationService _navigationService;
        private DelegateCommand _selectEventoCommand;

        public DelegateCommand SelectEventoCommand => _selectEventoCommand ?? (_selectEventoCommand = new DelegateCommand(ExecuteAddEdit));
        public EventoVM(INavigationService navigationService)
        {
            _navigationService = navigationService;
        }

        private async void ExecuteAddEdit()
        {
            //var parameters = new NavigationParameters();
            //parameters.Add("color", this);   
            //await _navigationService.NavigateAsync("EventosDatosPage", parameters);
            var EventoFechaVM = EventoFechaPageViewModel.GetInstance();
            EventoFechaVM.NuevoEvento = false;
            EventoFechaVM.EventoSeleccionado = this;
            await _navigationService.NavigateAsync("EventosDatosPage");
        }
    }
}
