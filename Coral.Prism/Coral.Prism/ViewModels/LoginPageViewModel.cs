﻿using Coral.Prism.Services.Interfaces;
using Coral.Prism.Models;
using Coral.Prism.Helper;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace Coral.Prism.ViewModels
{
    public class LoginPageViewModel : ViewModelBase
    {
        #region Propiedades Privadas
        private readonly INavigationService _navigationService;
        private readonly IApiService _apiService;
        private readonly IDialogService _dialogService;
        private DelegateCommand _recuperarPassCommand;
        private DelegateCommand _MainCommand;

        private string _errorTextEmail;
        private bool _hasErrorEmail;
        private string _errorTextPasword;
        private bool _hasErrorPasword;
        private string _email;
        private string _password;
        private bool _isRunning;
        private bool _isEnable;
        #endregion

        #region Propiedades publicas
        public string Email
        {
            get => _email;
            set => SetProperty(ref _email, value);
        }

        public string Password
        {
            get => _password;
            set => SetProperty(ref _password, value);
        }

        public string ErrorTextEmail
        {
            get => _errorTextEmail;
            set => SetProperty(ref _errorTextEmail, value);
        }

        public bool HasErrorEmail
        {
            get => _hasErrorEmail;
            set => SetProperty(ref _hasErrorEmail, value);
        }


        public string ErrorTexPassword
        {
            get => _errorTextPasword;
            set => SetProperty(ref _errorTextPasword, value);
        }

        public bool HasErrorPassword
        {
            get => _hasErrorPasword;
            set => SetProperty(ref _hasErrorPasword, value);
        }

        public bool IsRunning
        {
            get => _isRunning;
            set => SetProperty(ref _isRunning, value);
        }

        public bool IsEnable
        {
            get => _isEnable;
            set => SetProperty(ref _isEnable, value);
        }
        #endregion


        public DelegateCommand RecuperarCommand => _recuperarPassCommand ?? (_recuperarPassCommand = new DelegateCommand(ExecuteRecuperarCommand));
        public DelegateCommand LoginCommand => _MainCommand ?? (_MainCommand = new DelegateCommand(ExecuteLoginCommand));


        public LoginPageViewModel(INavigationService navigationService, IApiService apiService, IDialogService dialogService, IEventAggregator eventAggregator) : base(navigationService, eventAggregator)
        {
            _navigationService = navigationService;
            _apiService = apiService;
            _dialogService = dialogService;
        }

        async void ExecuteRecuperarCommand()
        {
            await _navigationService.NavigateAsync("RecuperarContrasena");            
        }


        async void ExecuteLoginCommand()
        {
            if (string.IsNullOrEmpty(Email))
            {
                ErrorTextEmail = "Es requerido";
                HasErrorEmail = true;
                return;
            }
            else
            {
                HasErrorEmail = false;
            }


            if (string.IsNullOrEmpty(Password))
            {
                ErrorTexPassword = "Es requerido";
                HasErrorPassword = true;
                return;
            }
            else
            {
                HasErrorEmail = false;
            }

            IsRunning = true;
            IsEnable = false;

            var request = new LoginRequest
            {
                codigo = Email,
                pw = Password
            };

            var url = App.Current.Resources["UrlAPI"].ToString();
            var _app = App.Current.Resources["UrlApp"].ToString();
            var response = await _apiService.GetLoginAsync(url, _app, "/Clientes", request);

            IsRunning = false;
            IsEnable = true;

            if (!response.IsSuccess)
            {
                await App.Current.MainPage.DisplayAlert("Error", response.Message.ToString(), "Ok");
                //CustomDialog.ShowAlert(_dialogService, "Error", response.Message.ToString());
                return;
            }

            LoginResponse user = response.Result;
            Settings.User = JsonConvert.SerializeObject(user.cliente);
            Settings.IsLogin = true;
            Settings.Rol = user.cliente.TipoRol;
            Application.Current.Properties["IsLoggedIn"] = Boolean.TrueString;
            var result = await NavigationService.NavigateAsync("/MasterDetailShellPage/NavigationPage/MainPage");

        }






    }
}
