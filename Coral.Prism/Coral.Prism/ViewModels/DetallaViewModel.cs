﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Coral.Prism.ViewModels
{
    public class DetallaViewModel
    {
 

        public string Descripcion { get; set; }
        public int IdPregunta { get; set; }        
        public List<OpinionViewModel> Opiniones { get; set; }
        public string TextoRespuesta { get; set; }
        //--------
        private OpinionViewModel _opcionSeleccionada { get; set; }

        public OpinionViewModel OpcionSeleccionda
        {

            get { return _opcionSeleccionada; }
            set
            {
                if (_opcionSeleccionada != value)
                {
                    _opcionSeleccionada = value;             
                }
            }

        }




        //--------
        public string PiePregunta { get; set; }

        public bool DetallePreguntaAbierta
        { 
            
            get {
                int countAbierts = Opiniones.Where(x => x.TextoAbierto == true).Count();
                return (countAbierts > 0);
            }        
        }
        
        public bool DetallePreguntaOpcional
        {
            get
            {
                int countAbierts = Opiniones.Where(x => x.TextoAbierto == true).Count();
                return (countAbierts == 0);
            }
        }

    }
}
