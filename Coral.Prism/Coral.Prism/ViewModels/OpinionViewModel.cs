﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Coral.Prism.ViewModels
{
    public class OpinionViewModel
    {        
        public int IdTipoOpinion { get; set; }
        public int IdRespuesta { get; set; }
        public string Titulo { get; set; }
        public bool TextoAbierto { get; set; }
        
    }
}
