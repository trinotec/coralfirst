﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Coral.Prism.ViewModels
{
    public class CambiarContrasenaRequest
    {
        public string CodCliente { get; set; }
        public string Email { get; set; }
        public string Clave { get; set; }
    }
}
