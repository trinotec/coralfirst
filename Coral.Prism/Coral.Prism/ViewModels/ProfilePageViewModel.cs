﻿using Coral.Prism.Helper;
using Coral.Prism.Models;
using Coral.Prism.Services.Interfaces;
using FFImageLoading;
using FFImageLoading.Forms;
using Newtonsoft.Json;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Xamarin.Forms;

namespace Coral.Prism.ViewModels
{
    public class ProfilePageViewModel : ViewModelBase
    {

        private readonly INavigationService _navigationService;
        private readonly IApiService _apiService;
        private readonly IDialogService _dialogService;
        private DelegateCommand _closeModalCommand;
        private DelegateCommand _saveCommand;
        private DelegateCommand _changeImageCommand;

        private string _codCliente;
        private string _nombre;
        private string _email;
        private DateTime _fechaNacimiento;
        private int _genero;
        private string _telefono;
        private bool _active;
        private float _montoConsumoBajo;
        private bool _notificacionCadaConsumo;
        private ImageSource _foto;
        private string _fotostream;
        private MediaFile _file;

        private bool _hasSwitch;
        private bool _isRunning;
        private bool _isEnable;

        #region Propiedades Publicos
        public string Nombre
        {
            get => _nombre;
            set => SetProperty(ref _nombre, value);
        }
        public string Email
        {
            get => _email;
            set => SetProperty(ref _email, value);
        }

        public DateTime FechaNacimiento
        {
            get => _fechaNacimiento;
            set => SetProperty(ref _fechaNacimiento, value);
        }

        public int Genero
        {
            get => _genero;
            set => SetProperty(ref _genero, value);
        }

        public string Telefono
        {
            get => _telefono;
            set => SetProperty(ref _telefono, value);
        }

        public bool Active
        {
            get => _active;
            set => SetProperty(ref _active, value);
        }

        public bool HasSwitch
        {
            get => _hasSwitch;
            set => SetProperty(ref _hasSwitch, value);
        }

        public ImageSource Foto
        {
            get => _foto;
            set => SetProperty(ref _foto, value);
        }

        public string FotoStream
        {
            get => _fotostream;
            set => SetProperty(ref _fotostream, value);
        }

        public bool IsRunning
        {
            get => _isRunning;
            set => SetProperty(ref _isRunning, value);
        }

        public bool IsEnable
        {
            get => _isEnable;
            set => SetProperty(ref _isEnable, value);
        }
        #endregion

        public DelegateCommand CloseModalCommand => _closeModalCommand ?? (_closeModalCommand = new DelegateCommand(ExecuteCloseCommand));
        public DelegateCommand SaveCommand => _saveCommand ?? (_saveCommand = new DelegateCommand(ExecuteSaveCommand));
        public DelegateCommand ChangeImageCommand => _changeImageCommand ?? (_changeImageCommand = new DelegateCommand(ChangeImage));

        public ProfilePageViewModel(INavigationService navigationService, IApiService apiService, IDialogService dialogService,IEventAggregator eventAggregator) : base(navigationService, eventAggregator)
        {
            _navigationService = navigationService;
            _apiService = apiService;
            _dialogService = dialogService;
            SetProfile();
        }

        async void ExecuteCloseCommand()
        {
            await _navigationService.GoBackAsync();
        }

        private async void ExecuteSaveCommand()
        {
            IsRunning = true;
            IsEnable = false;
            string genero = Genero == 0 ? "M" : "F";

            UserRequest request;

            if (_file != null)
            {
                byte[] imageArray = null;
                imageArray = FilesHelper.ReadFully(_file.GetStream());
                request = new UserRequest
                {
                    CodCliente = _codCliente,
                    Nombre = Nombre,
                    Email = Email,
                    Activo = true,
                    NotificacionConsumoBajo = Active,
                    FechaCumpleAnos = FechaNacimiento.ToString("MM/dd/yyyy"),
                    Telefono = Telefono,
                    Genero = genero,
                    ImagenBase64 = Convert.ToBase64String(imageArray, 0, imageArray.Length)
                };
            }
            else
            {
                request = new UserRequest
                {
                    CodCliente = _codCliente,
                    Nombre = Nombre,
                    Email = Email,
                    Activo = true,
                    NotificacionConsumoBajo = Active,
                    FechaCumpleAnos = FechaNacimiento.ToString("MM/dd/yyyy"),
                    Telefono = Telefono,
                    Genero = genero,
                    ImagenBase64 = string.Empty
                };
            }

            var url = App.Current.Resources["UrlAPI"].ToString();
            var _app = App.Current.Resources["UrlApp"].ToString();
            var response = await _apiService.UpdateProfile(url, _app, "/Clientes/ActualizarCliente", request);

            IsRunning = false;
            IsEnable = true;

            if (!response.IsSuccess)
            {
                //CustomDialog.ShowAlert(_dialogService, "Error", response.Message.ToString());
                await App.Current.MainPage.DisplayAlert("Error", response.Message.ToString(), "Ok");
                return;
            }

            var userResponse = response.Result;

            var user = new Cliente
            {
                CodCliente = _codCliente,
                CambiarClave = false,
                Email = Email,
                FechaCumpleAnos = FechaNacimiento.ToString("MM-dd-yyyy"),
                Genero = genero,
                MontoConsumoBajo = _montoConsumoBajo,
                Nombre = Nombre,
                Telefono = Telefono,
                NotificacionConsumoBajo = Active,
                NotificacionCadaConsumo = _notificacionCadaConsumo,
                Foto = userResponse.Imagen
            };

            Settings.User = JsonConvert.SerializeObject(user);

            MasterDetailShellPageViewModel.GetInstance().Foto = new UriImageSource
            {
                Uri = new Uri(userResponse.Imagen),
                CachingEnabled = false,
                CacheValidity = new TimeSpan(1, 0, 0, 0)
            };
            MasterDetailShellPageViewModel.GetInstance().Nombre = Nombre;

            await App.Current.MainPage.DisplayAlert("Cambio Realizado", "Se actualizo la información correctamente", "Ok");
            //CustomDialog.ShowAlert(_dialogService, "Cambio Realizado", "Se actualizo la información correctamente", "success");

        }



        private async void ChangeImage()
        {
            await CrossMedia.Current.Initialize();
            var source = await Application.Current.MainPage.DisplayActionSheet(
               "¿De donde quiere obtener la foto?",
               "Cancelar",
               null,
               "Galería",
               "Cámara");

            if (source == "Cancelar")
            {
                _file = null;
                return;
            }
            if (source == "Cámara")
            {
                _file = await CrossMedia.Current.TakePhotoAsync(
                    new StoreCameraMediaOptions
                    {
                        Directory = "PerfilComedor",
                        Name = "miperfil.png",
                        PhotoSize = PhotoSize.Small,
                    }
                );
            }
            else
            {
                _file = await CrossMedia.Current.PickPhotoAsync();
            }

            if (_file != null)
            {
                Foto = ImageSource.FromStream(() =>
                {
                    var stream = _file.GetStream();
                    return stream;
                });
            }

        }

        private async void SetProfile()
        {
            var user = JsonConvert.DeserializeObject<Cliente>(Settings.User);
            _codCliente = user.CodCliente;
            _montoConsumoBajo = user.MontoConsumoBajo;
            _notificacionCadaConsumo = user.NotificacionCadaConsumo;
            //await CachedImage.InvalidateCache(Foto, FFImageLoading.Cache.CacheType.Memory, false);
            Foto = null;
            Foto = new UriImageSource
            {
                Uri = new Uri(user.Foto),
                CachingEnabled = false,

            };
            await ImageService.Instance.InvalidateCacheAsync(FFImageLoading.Cache.CacheType.All);
            Nombre = user.Nombre;
            Email = user.Email;
            FechaNacimiento = DateTime.ParseExact(user.FechaCumpleAnos, "MM-dd-yyyy", CultureInfo.InvariantCulture);
            Genero = user.Genero == "M" ? 0 : 1;
            Telefono = user.Telefono;
            Active = user.NotificacionConsumoBajo;
            HasSwitch = true;
           
        }
    }
}
