﻿using Coral.Prism.Helper;
using Coral.Prism.Models;
using Coral.Prism.Services.Interfaces;
using Newtonsoft.Json;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;

namespace Coral.Prism.ViewModels
{
    public class NoticiasPageViewModel :ViewModelBase 
    {
        private static System.Timers.Timer aTimer;

        private readonly INavigationService _navigationService;
        private readonly IApiService _apiService;
        private readonly IDialogService _dialogService;
        public IPageDialogService PageDialogService { get; }
        public IPopupService PopupService { get; }

        private ObservableCollection<NoticiaItemViewModel> _noticiasMasleidas;
        private ObservableCollection<NoticiaItemViewModel> _noticiasRecientes;

        public ObservableCollection<NoticiaItemViewModel> NoticiasMasLeidas
        {
            get => _noticiasMasleidas;
            set => SetProperty(ref _noticiasMasleidas, value);
        }

        public ObservableCollection<NoticiaItemViewModel> NoticiasRecientes
        {
            get => _noticiasRecientes;
            set => SetProperty(ref _noticiasRecientes, value);
        }

        private bool _isRunning;
        private bool _isVisible;
        public bool IsRunning
        {
            get => _isRunning;
            set => SetProperty(ref _isRunning, value);
        }

        public bool IsVisible
        {
            get => _isVisible;
            set => SetProperty(ref _isVisible, value);
        }


        public NoticiasPageViewModel(INavigationService navigationService, IApiService apiService, IDialogService dialogService, IEventAggregator eventAggregator) : base(navigationService, eventAggregator)
        {
            _navigationService = navigationService;
            _apiService = apiService;
            _dialogService = dialogService;
            IsFabButtonVisible = false;
            SelectedMenu = 0;
            IsRunning = true;
            IsVisible = false;

        }

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);
            var data = _navigationService.GetNavigationUriPath();
            var navigationMode = parameters.GetNavigationMode();
            if (navigationMode == NavigationMode.New)
            {
                SetTimer();
            }
        }




        private void LoadDataNews()
        {
            IsRunning = true;
            IsVisible = false;

            try
            {
                //var url = App.Current.Resources["UrlAPI"].ToString();
                //var response = await _apiService.GetListNoticias(url, "WebApiCorporativo/api", "/Noticias");
                //if (!response.IsSuccess)
                //{
                //    IsRunning = false;
                //    CustomDialog.ShowAlert(_dialogService, "Error", response.Message.ToString());
                //    return;
                //}

                //var _data = (ICollection<Noticia>)response.Result;
                if (!string.IsNullOrEmpty(Settings.News))
                {
                    aTimer.Stop();
                    var _data = JsonConvert.DeserializeObject<ICollection<Noticia>>(Settings.News);

                    NoticiasRecientes = new ObservableCollection<NoticiaItemViewModel>(
                        _data.Select(n => new NoticiaItemViewModel(_navigationService)
                        {
                            Autor = n.Autor,
                            Fecha = n.Fecha,
                            Titulo = n.Titulo,
                            Detalle = n.Detalle,
                            FechaCreacion = n.FechaCreacion,
                            FechaPublicacion = n.FechaPublicacion,
                            IdNoticia = n.IdNoticia,
                            Tipo = n.Tipo,
                            url = n.url
                        }).ToList());

                    IsRunning = false;
                    IsVisible = true;
                }
            }
            catch (Exception ex)
            {
                IsRunning = false;
                var msg = ex.Message;
                IsVisible = true;
            }
        }

        private void SetTimer()
        {
            // Create a timer with a two second interval.
            aTimer = new System.Timers.Timer(2000);
            // Hook up the Elapsed event for the timer. 
            aTimer.Elapsed += OnTimedEvent;
            aTimer.AutoReset = true;
            aTimer.Enabled = true;
        }

        private void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            Console.WriteLine("The Elapsed event was raised at {0:HH:mm:ss.fff}",
                              e.SignalTime);
            LoadDataNews();
        }



    }
}
