﻿using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using Coral.Prism.Services.Interfaces;
using System.Linq;
using Newtonsoft.Json;
using Coral.Prism.Helper;
using Coral.Prism.Models;
using System.Collections.ObjectModel;
using Prism.AppModel;
using Syncfusion.SfCalendar.XForms;
using Xamarin.Forms;

namespace Coral.Prism.ViewModels
{
    public class EventoFechaPageViewModel : ViewModelBase, IPageLifecycleAware
    {
        private readonly INavigationService _navigationService;
        private DelegateCommand _navigateCommand;
        private readonly IApiService _apiService;


        private static EventoFechaPageViewModel _instance;
        private DateTime _selectedDate;
        public DateTime SelectedDate
        {
            get => _selectedDate;
            set => SetProperty(ref _selectedDate, value);

        }

        private DateTime _fechaActual;
        public DateTime FechaActual
        {
            get => _fechaActual;
            set => SetProperty(ref _fechaActual, value);
        }

        private bool _isVisibleEvento;
        public bool IsVisibleEvento
        {
            get => _isVisibleEvento;
            set => SetProperty(ref _isVisibleEvento, value);
        }

        private ObservableCollection<Evento> _eventos;
        public ObservableCollection<Evento> Eventos
        {
            get => _eventos;
            set => SetProperty(ref _eventos, value);
        }

        private ObservableCollection<EventoVM> _eventosMes;
        public ObservableCollection<EventoVM> EventosMes
        {
            get => _eventosMes;
            set => SetProperty(ref _eventosMes, value);
        }

        
        private EventoVM _eventoSeleccionado;
        public EventoVM EventoSeleccionado
        {
            get => _eventoSeleccionado;
            set => SetProperty(ref _eventoSeleccionado, value);
        }

        public bool NuevoEvento { get; set; }

        private bool _isRunning;
        public bool IsRunning
        {
            get => _isRunning;
            set => SetProperty(ref _isRunning, value);
        }

        private int _heightEventos;
        public int HeightEventos
        {
            get => _heightEventos;
            set => SetProperty(ref _heightEventos, value);
        }


        public DelegateCommand NavigateCommand => _navigateCommand ?? (_navigateCommand = new DelegateCommand(ExecuteDatosEvento));
        public EventoFechaPageViewModel(INavigationService navigationService, IEventAggregator eventAggregator, IApiService apiService) : base(navigationService, eventAggregator)
        {
            _navigationService = navigationService;
            _apiService = apiService;
            _instance = this;
            FechaActual = DateTime.Now;
            IsVisibleEvento = false;
            IsRunning = true;
            HeightEventos = 0;
            SelectedDate = new DateTime();
            

        }

        public static EventoFechaPageViewModel GetInstance()
        {
            return _instance;
        }

        public async void ExecuteDatosEvento()
        {
            if (SelectedDate.Year > 1)
            {
                NuevoEvento = true;
                await _navigationService.NavigateAsync("EventosDatosPage");
            }
            else
            {
                
                await App.Current.MainPage.DisplayAlert("Error", "Seleccione una fecha", "Ok");
            }

            
        }

        public async void LoadEventos()
        {
            IsRunning = true;
            var user = JsonConvert.DeserializeObject<Cliente>(Settings.User);
            var url = App.Current.Resources["UrlAPI"].ToString();
            var _app = App.Current.Resources["UrlApp"].ToString();

            var response = await _apiService.GetListEventos(url, _app, "/Eventos/Filtrar", user.Email);
            if (!response.IsSuccess)
            {
                IsRunning = false;
                await App.Current.MainPage.DisplayAlert("Error", response.Message.ToString(), "Ok");
                return;
            }
            IsRunning = false;
            Eventos = new ObservableCollection<Evento>();
            if (response.Result.Count > 0)
            {
                response.Result.ForEach(x => Eventos.Add(x));
                if (SelectedDate.Year > 1)
                {
                    MostrarEventosDias();
                }
                //MostrarEventosMes(DateTime.Now.Month);
                loadEventosCalendario();
            }
        }


        public void OnAppearing()
        {
            LoadEventos();            
        }

        public void OnDisappearing()
        {
            //throw new NotImplementedException();
        }

        public CalendarEventCollection CalendarInlineEvents { get; set; } = new CalendarEventCollection();

        public void loadEventosCalendario(){

            foreach (var EventoxX in Eventos)
            {
                CalendarInlineEvent evenTempX1 = new CalendarInlineEvent()
                {
                    StartTime = EventoxX.HoraInicio,
                    EndTime = EventoxX.HoraFinal,
                    Subject = EventoxX.NombreEvento,
                    Color = Color.Fuchsia
                };
                CalendarInlineEvents.Add(evenTempX1);
            }
        }

        public void MostrarEventosMes(int Mes)
        {
            if (Eventos != null)
            {
                EventosMes = new ObservableCollection<EventoVM>();
                var EveMe = Eventos.Where(x => x.HoraInicio.Month == Mes).ToList();
                if (EveMe.Count > 0)
                {
                    foreach (var x in EveMe.OrderBy(x => x.HoraInicio).ToList())
                    {
                        EventosMes.Add(new EventoVM(_navigationService) {
                            CantPersonas = x.CantPersonas,
                            CodCliente = x.CodCliente,
                            DescripcionLarga = x.DescripcionLarga,
                            FechaIngreso = x.FechaIngreso,
                            HoraFinal = x.HoraFinal,
                            HoraInicio = x.HoraInicio,
                            idEvento = x.idEvento,
                            NombreEvento = x.NombreEvento,
                            NomCliente = x.NomCliente
                        });
                    }                    
                    HeightEventos = 100 * EveMe.Count;
                }
            }            
        }


        public void MostrarEventosDias()
        {           
            if (Eventos != null)
            {
                EventosMes = new ObservableCollection<EventoVM>();
                var EveMe = Eventos.Where(x => SelectedDate.Month == x.HoraInicio.Month && SelectedDate.Day == x.HoraInicio.Day && SelectedDate.Year == x.HoraInicio.Year).OrderBy(x=>x.HoraInicio).ToList();
                if (EveMe.Count > 0)
                {
                    foreach (var x in EveMe.OrderBy(x => x.HoraInicio).ToList())
                    {
                        EventosMes.Add(new EventoVM(_navigationService)
                        {
                            CantPersonas = x.CantPersonas,
                            CodCliente = x.CodCliente,
                            DescripcionLarga = x.DescripcionLarga,
                            FechaIngreso = x.FechaIngreso,
                            HoraFinal = x.HoraFinal,
                            HoraInicio = x.HoraInicio,
                            idEvento = x.idEvento,
                            NombreEvento = x.NombreEvento,
                            NomCliente = x.NomCliente
                        });
                    }
                    HeightEventos = 100 * EveMe.Count;
                }
            }
        }
    }
}
