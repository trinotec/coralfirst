﻿using Coral.Prism.Helper;
using Coral.Prism.Services.Interfaces;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Coral.Prism.ViewModels
{
    public class CambiarContrasenaViewModel : ViewModelBase
    {
        private readonly INavigationService _navigationService;
        private readonly IApiService _apiService;
        private readonly IDialogService _dialogService;
        private readonly IPageDialogService _pageDialogService;
        private DelegateCommand _navigateCommand;
        private DelegateCommand _CancelCommand;
        private bool _isRunning;
        private bool _isEnable;
        private string _password;
        private string _repPassword;
        private string _errorTextPassword;
        private bool _hasErrorPassword;
        private string _errorTextRepPassword;
        private bool _hasErrorRepPassword;

        public DelegateCommand NavigateMain => _navigateCommand ?? (_navigateCommand = new DelegateCommand(ExecuteNavigateMain));
        public DelegateCommand CancelCambiarPass => _CancelCommand ?? (_CancelCommand = new DelegateCommand(ExecuteNavigateLogin));

        public string Usuario { get; set; }

        public string Password
        {
            get => _password;
            set => SetProperty(ref _password, value);
        }

        public string RepPassword
        {
            get => _repPassword;
            set => SetProperty(ref _repPassword, value);
        }

        public string ErrorTextPassword
        {
            get => _errorTextPassword;
            set => SetProperty(ref _errorTextPassword, value);
        }

        public bool HasErrorPassword
        {
            get => _hasErrorPassword;
            set => SetProperty(ref _hasErrorPassword, value);
        }


        public string ErrorTextRepPassword
        {
            get => _errorTextRepPassword;
            set => SetProperty(ref _errorTextRepPassword, value);
        }

        public bool HasErrorRepPassword
        {
            get => _hasErrorRepPassword;
            set => SetProperty(ref _hasErrorRepPassword, value);
        }

        public bool IsRunning
        {
            get => _isRunning;
            set => SetProperty(ref _isRunning, value);
        }

        public bool IsEnable
        {
            get => _isEnable;
            set => SetProperty(ref _isEnable, value);
        }


        public CambiarContrasenaViewModel(INavigationService navigationService, IApiService apiService, IDialogService dialogService, IEventAggregator eventAggregator, IPageDialogService pageDialogService) : base(navigationService, eventAggregator)
        {
            _navigationService = navigationService;
            _apiService = apiService;
            _dialogService = dialogService;
            _pageDialogService = pageDialogService;
            IsEnable = true;
        }

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            Usuario = parameters.GetValue<string>("Usuario");
        }


        
        async void ExecuteNavigateMain()
        {

            if (string.IsNullOrEmpty(Password))
            {
                ErrorTextPassword = "Es requerido";
                HasErrorPassword = true;
                return;
            }
            else
            {
                HasErrorPassword = false;
            }


            if (string.IsNullOrEmpty(RepPassword))
            {
                ErrorTextRepPassword = "Es requerido";
                HasErrorRepPassword = true;
                return;
            }
            else
            {
                HasErrorRepPassword = false;
            }


            if (Password != RepPassword)
            {                
                //CustomDialog.ShowAlert(_dialogService, "Error", "Las contraseñas no coinciden");
                await _pageDialogService.DisplayAlertAsync("Error", "Las contraseñas no coinciden", "Ok");
                return;
            }            

            IsRunning = true;
            IsEnable = false;

            var CodSegVM = CodigoSeguridadViewModel.GetInstance();
            
            var request = new CambiarContrasenaRequest
            {
                CodCliente = Usuario,
                Email = Usuario,
                Clave = Password

            };

            var url = App.Current.Resources["UrlAPI"].ToString();
            var _app = App.Current.Resources["UrlApp"].ToString();
            //var response = await _apiService.UpdateContrasena(url, _app, "/Clientes/CambiarClave", request);
            var response = await _apiService.UpdateContrasena(url, _app, "/Clientes/RecuperarClave", request, CodSegVM.Codigo);

            IsRunning = false;
            IsEnable = true;

            if (!response.IsSuccess)
            {
                //await App.Current.MainPage.DisplayAlert("Error", response.Message.ToString(), "Ok");
                //CustomDialog.ShowAlert(_dialogService, "Error", response.Message.ToString());
                await _pageDialogService.DisplayAlertAsync("Error", response.Message.ToString(), "Ok");
                return;
            }

            if(response.Result.Success == false)
            {                
                //CustomDialog.ShowAlert(_dialogService, "Error", "No se pudo cambiar la contraseña");
                await _pageDialogService.DisplayAlertAsync("Error", "No se pudo cambiar la contraseña", "Ok");
                return;
            }

            await App.Current.MainPage.DisplayAlert("Cambio Realizado", "Se cambio la contraseña correctamente", "Ok");
            await _navigationService.NavigateAsync("/LoginPage"); 
        } 

        async void ExecuteNavigateLogin()
        {
            await _navigationService.NavigateAsync("/LoginPage");            
        }


    }
}
