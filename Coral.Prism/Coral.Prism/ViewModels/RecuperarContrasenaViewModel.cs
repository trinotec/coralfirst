﻿using Coral.Prism.Services.Interfaces;
using Coral.Prism.Models;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using Coral.Prism.Helper;

namespace Coral.Prism.ViewModels
{
    public class RecuperarContrasenaViewModel : ViewModelBase
    {
        private readonly INavigationService _navigationService;
        private readonly IApiService _apiService;
        private readonly IDialogService _dialogService;
        private DelegateCommand _recuperarCommand;


        private string _email;
        private string _errorTextEmail;
        private bool _hasErrorEmail;
        private bool _isRunning;
        private bool _isEnable;


        public string Email
        {
            get => _email;
            set => SetProperty(ref _email, value);
        }

        public string ErrorTextEmail
        {
            get => _errorTextEmail;
            set => SetProperty(ref _errorTextEmail, value);
        }

        public bool HasErrorEmail
        {
            get => _hasErrorEmail;
            set => SetProperty(ref _hasErrorEmail, value);
        }

        public bool IsRunning
        {
            get => _isRunning;
            set => SetProperty(ref _isRunning, value);
        }

        public bool IsEnable
        {
            get => _isEnable;
            set => SetProperty(ref _isEnable, value);
        }

        public DelegateCommand RecuperarCommand => _recuperarCommand ?? (_recuperarCommand = new DelegateCommand(ExecuteRecuperarCommand));

        public RecuperarContrasenaViewModel(INavigationService navigationService, IApiService apiService, IDialogService dialogService, IEventAggregator eventAggregator) : base(navigationService, eventAggregator)
        {
            _navigationService = navigationService;
            _apiService = apiService;
            _dialogService = dialogService;
        }

        async void ExecuteRecuperarCommand()
        {
            if (string.IsNullOrEmpty(Email))
            {
                ErrorTextEmail = "Es requerido";
                HasErrorEmail = true;
                return;
            }
            else
            {
                HasErrorEmail = false;
            }
            //-----------------------------------------------------

            IsRunning = true;
            IsEnable = false;

            var request = new CodeRecuperacionRequest
            {
                codigo = Email
            };

            var url = App.Current.Resources["UrlAPI"].ToString();
            var _app = App.Current.Resources["UrlApp"].ToString();
            var response = await _apiService.GetCodeRecuperacionAsync(url, _app, "/Clientes/SolicitaRecuperarClave", request);

            IsRunning = false;
            IsEnable = true;

            if (!response.IsSuccess)
            {
                //await App.Current.MainPage.DisplayAlert("Error", response.Message.ToString(), "Ok");
                CustomDialog.ShowAlert(_dialogService, "Error", response.Message.ToString());
                return;
            }

            CodeRecuperacionResponse Codigo = response.Result;

            //-----------------------------------------------------
            var parametrosEnviar = new NavigationParameters();
            parametrosEnviar.Add("Usuario", Email);
            parametrosEnviar.Add("CodigoWs", Codigo.CodigoRecuperacion);

            await _navigationService.NavigateAsync("CodigoSeguridad", parametrosEnviar);
        }

    }
}
