﻿using Coral.Prism.Helper;
using Coral.Prism.Models;
using Coral.Prism.Services.Interfaces;
using Coral.Prism.Views;
using Newtonsoft.Json;
using Prism.AppModel;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Coral.Prism.ViewModels
{
    public class EventosDatosPageViewModel : ViewModelBase, IPageLifecycleAware, INavigationAware
    {
        private readonly INavigationService _navigationService;
        private readonly IApiService _apiService;
        private DelegateCommand _navigateCommand;
        
        private string _nombreEvento;
        public string NombreEvento  
        {
            get { return _nombreEvento; }  
            set { _nombreEvento = value; }  
        }

        private double _numPersonas;
        public double NumPersonas  
        {
            get { return _numPersonas; }  
            set { _numPersonas = value; }  
        }

        private string _horariosEvento;
        public string HorariosEvento 
        {
            get { return _horariosEvento; }  
            set { _horariosEvento = value; }  
        }

       
        private string _descripcionEvento;
        public string DescripcionEvento 
        {
            get { return _descripcionEvento; }  
            set { _descripcionEvento = value; }  
        }

        private TimeSpan _horaInicio;
        public TimeSpan HoraInicio  
        {
            get { return _horaInicio; }  
            set { _horaInicio = value; }  
        }

        private TimeSpan _horaFin;
        public TimeSpan HoraFin  
        {
            get { return _horaFin; }  
            set { _horaFin = value; }  
        }

        private bool _isRunning;
        public bool IsRunning
        {
            get => _isRunning;
            set => SetProperty(ref _isRunning, value);
        }

        private bool _isEnable;
        public bool IsEnable
        {
            get => _isEnable;
            set => SetProperty(ref _isEnable, value);
        }

        private bool _isVisibleFrmDatosPrincipal;
        public bool IsVisibleFrmDatosPrincipal
        {
            get => _isVisibleFrmDatosPrincipal;
            set => SetProperty(ref _isVisibleFrmDatosPrincipal, value);
        }


        private bool _isVisibleFrmHorarios;
        public bool IsVisibleFrmHorarios
        {
            get => _isVisibleFrmHorarios;
            set => SetProperty(ref _isVisibleFrmHorarios, value);
        }

        private bool _isVisiblefrmGuardado;
        public bool IsVisiblefrmGuardado
        {
            get => _isVisiblefrmGuardado;
            set => SetProperty(ref _isVisiblefrmGuardado, value);
        }


        

        private string _tituloVista;
        public string TituloVista
        {
            get => _tituloVista;
            set => SetProperty(ref _tituloVista, value);
        }


        private string _botonTexto;
        public string BotonTexto
        {
            get => _botonTexto;
            set => SetProperty(ref _botonTexto, value);
        }

        public EventoFechaPageViewModel EventoFechaVM { get; set; }

        public bool NuevoEvento { get; set; }

        public DelegateCommand NavigateCommand => _navigateCommand ?? (_navigateCommand = new DelegateCommand(ExecuteDatosEvento));
        public EventosDatosPageViewModel(INavigationService navigationService, IEventAggregator eventAggregator, IApiService apiService) : base(navigationService, eventAggregator)
        {
            IsRunning = false;
            IsEnable = true;
            _navigationService = navigationService;
            _apiService = apiService;

            IsVisibleFrmDatosPrincipal = true;
            IsVisibleFrmHorarios = false;
            IsVisiblefrmGuardado = false;
            EventoFechaVM = EventoFechaPageViewModel.GetInstance();
            SetDataInitial();
                     
        }

        public  void MosTrarEstablecerHorarios()
        {
            IsVisibleFrmHorarios = true;
            IsVisibleFrmDatosPrincipal = false;
        }

        public  void EstablecerHorarios()
        {

            if (HoraInicio >= HoraFin)
            {
                 App.Current.MainPage.DisplayAlert("Horarios Incorrectos", "Verifique la hora inicio y fin del evento", "Ok");
            }
            else
            {
                var HoraIni = HelpFormat.FormatoFecha(HoraInicio);
                var HoraF = HelpFormat.FormatoFecha(HoraFin);                
                HorariosEvento = $"{HoraIni} - {HoraF}";                
                IsVisibleFrmHorarios = false;
                IsVisibleFrmDatosPrincipal = true;                
            }
        }

        public async void ExecuteDatosEvento()
        {
            if (string.IsNullOrEmpty(NombreEvento) == true || NumPersonas <= 0 || string.IsNullOrEmpty(HorariosEvento) == true || string.IsNullOrEmpty(DescripcionEvento) == true || (NumPersonas % 1 == 0) == false)
            {

                if ((NumPersonas % 1 == 0) == false)
                {
                    await App.Current.MainPage.DisplayAlert("Error", "Agregue una cantidad valida de invitados", "Ok");
                    return;
                }
                await App.Current.MainPage.DisplayAlert("Error", "Todos los campos son obligatorios", "Ok");
                return;
            }

            if (EventoFechaVM.NuevoEvento)
            {
                GuardarEvento();
            }
            else
            {
                EditarEvento();
            }
        }


        public async void GuardarEvento()
        {            
            var fechaEvento = EventoFechaVM.SelectedDate;
            IsRunning = true;
            IsEnable = false;
            DateTime horaIni = new System.DateTime(fechaEvento.Year, fechaEvento.Month, fechaEvento.Day, HoraInicio.Hours, HoraInicio.Minutes, 0, 0);
            DateTime horaFin = new System.DateTime(fechaEvento.Year, fechaEvento.Month, fechaEvento.Day, HoraFin.Hours, HoraFin.Minutes, 0, 0);

            var user = JsonConvert.DeserializeObject<Cliente>(Settings.User);
            var request = new EventoRequest
            {
                CantPersonas = Convert.ToInt32(NumPersonas),
                CodCliente = user.Email,
                DescripcionLarga = DescripcionEvento,
                NombreEvento = NombreEvento,
                HoraInicio = horaIni,
                HoraFinal = horaFin
            };

            var url = App.Current.Resources["UrlAPI"].ToString();
            var _app = App.Current.Resources["UrlApp"].ToString();
            var response = await _apiService.SendEvento(url, _app, "/Eventos", request);

            if (!response.IsSuccess)
            {
                IsRunning = false;
                IsEnable = true;                
                await App.Current.MainPage.DisplayAlert("Error", response.Message.ToString(), "Ok");
                return;
            }
       
            IsRunning = false;
            IsEnable = true;
            
            var EventoFechasVM = EventoFechaPageViewModel.GetInstance();
            IsVisibleFrmDatosPrincipal = false;
            IsVisiblefrmGuardado = true;

            await Task.Delay(1000);
            await _navigationService.GoBackAsync();
        }

        public async void EditarEvento()
        {            
            var fechaEvento = EventoFechaVM.EventoSeleccionado.HoraInicio;
            IsRunning = true;
            IsEnable = false;
            DateTime horaIni = new System.DateTime(fechaEvento.Year, fechaEvento.Month, fechaEvento.Day, HoraInicio.Hours, HoraInicio.Minutes, 0, 0);
            DateTime horaFin = new System.DateTime(fechaEvento.Year, fechaEvento.Month, fechaEvento.Day, HoraFin.Hours, HoraFin.Minutes, 0, 0);

            var user = JsonConvert.DeserializeObject<Cliente>(Settings.User);
            var request = new EventoRequest
            {
                idEvento = EventoFechaVM.EventoSeleccionado.idEvento,
                CantPersonas = Convert.ToInt32(NumPersonas),
                CodCliente = user.Email,
                DescripcionLarga = DescripcionEvento,
                NombreEvento = NombreEvento,
                HoraInicio = horaIni,
                HoraFinal = horaFin
            };

            var url = App.Current.Resources["UrlAPI"].ToString();
            var _app = App.Current.Resources["UrlApp"].ToString();
            var response = await _apiService.EditEvento(url, _app, "/Eventos/Actualizar", request);

            if (!response.IsSuccess)
            {
                IsRunning = false;
                IsEnable = true;
                await App.Current.MainPage.DisplayAlert("Error", response.Message.ToString(), "Ok");
                return;
            }

            IsRunning = false;
            IsEnable = true;

            var EventoFechasVM = EventoFechaPageViewModel.GetInstance();
            IsVisibleFrmDatosPrincipal = false;
            IsVisiblefrmGuardado = true;

            await Task.Delay(1000);
            await _navigationService.GoBackAsync();
        }


        public void OnAppearing()
        {
            //throw new NotImplementedException();
        }

        public void OnDisappearing()
        {
            //throw new NotImplementedException();
        }

        public void SetDataInitial()
        {
            if (EventoFechaVM.NuevoEvento)
            {
                TituloVista = "Ingresar Evento";
                BotonTexto = "Crear Evento";
            }
            else
            {
                TituloVista = "Editar Evento";
                BotonTexto = "Editar Evento";                
                NombreEvento = EventoFechaVM.EventoSeleccionado.NombreEvento;
                NumPersonas = EventoFechaVM.EventoSeleccionado.CantPersonas;
                DescripcionEvento = EventoFechaVM.EventoSeleccionado.DescripcionLarga;

                var horainicio = new TimeSpan(EventoFechaVM.EventoSeleccionado.HoraInicio.Hour, EventoFechaVM.EventoSeleccionado.HoraInicio.Minute, 0);
                var horafin = new TimeSpan(EventoFechaVM.EventoSeleccionado.HoraFinal.Hour, EventoFechaVM.EventoSeleccionado.HoraFinal.Minute, 0);
                var HoraIni = HelpFormat.FormatoFecha(horainicio);
                var HoraF = HelpFormat.FormatoFecha(horafin);
                HoraInicio = horainicio;
                HoraFin = horafin;
                HorariosEvento = $"{HoraIni} - {HoraF}";

            }
        }

    }
}
