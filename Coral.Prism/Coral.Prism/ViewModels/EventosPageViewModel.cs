﻿using Coral.Prism.Services.Interfaces;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Coral.Prism.ViewModels
{
    public class EventosPageViewModel : ViewModelBase
    {
        private readonly INavigationService _navigationService;
        private readonly IApiService _apiService;
        private readonly IDialogService _dialogService;
        public IPageDialogService PageDialogService { get; }
        public IPopupService PopupService { get; }

        public EventosPageViewModel(IPopupService popupService, IPageDialogService pageDialogService, INavigationService navigationService, IEventAggregator eventAggregator, IApiService apiService, IDialogService dialogService) : base(navigationService, eventAggregator)
        {
            PageDialogService = pageDialogService;
            PopupService = popupService;
            IsFabButtonVisible = false;

            _navigationService = navigationService;
            _apiService = apiService;
            _dialogService = dialogService;
        }
    }
}
