﻿using Coral.Prism.Models;
using Prism.Commands;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Coral.Prism.ViewModels
{
    public class PlatilloItemViewModel : Comida
    {
        private readonly INavigationService _navigationService;
        private DelegateCommand _tablaNutricionCommand;

        public PlatilloItemViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;
        }

        public DelegateCommand TablaNutricionCommand => _tablaNutricionCommand ?? (_tablaNutricionCommand = new DelegateCommand(TablaNutricion));

        private void TablaNutricion()
        {

        }
    }
}
