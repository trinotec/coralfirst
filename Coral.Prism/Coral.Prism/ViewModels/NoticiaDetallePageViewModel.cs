﻿using Coral.Prism.Models;
using Coral.Prism.Services.Interfaces;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Coral.Prism.ViewModels
{

    public class NoticiaDetallePageViewModel : ViewModelBase
    {
        private Noticia _noticia;
        private bool _isRunning;
        private bool _isVisible;

        public Noticia Noticia
        {
            get => _noticia;
            set => SetProperty(ref _noticia, value);
        }

        public bool IsRunning
        {
            get => _isRunning;
            set => SetProperty(ref _isRunning, value);
        }

        public bool IsVisible
        {
            get => _isVisible;
            set => SetProperty(ref _isVisible, value);
        }


        public NoticiaDetallePageViewModel(INavigationService navigationService, IApiService apiService, IDialogService dialogService, IEventAggregator eventAggregator) : base(navigationService, eventAggregator)
        {
            IsRunning = true;
            IsVisible = false;
            IsFabButtonVisible = false;
        }


        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);
         
            if (parameters.ContainsKey("noticia"))
            {
                Noticia = parameters.GetValue<Noticia>("noticia");
            } 
        }

    }
}
