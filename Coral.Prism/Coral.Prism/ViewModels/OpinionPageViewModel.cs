﻿using Coral.Prism.Helper;
using Coral.Prism.Models;
using Coral.Prism.Services.Interfaces;
using Prism.Events;
using Prism.Navigation;
using Prism.Services;
using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Coral.Prism.ViewModels;
using System.Linq;
using Newtonsoft.Json;
using Prism.Commands;

namespace Coral.Prism.ViewModels
{
    public class OpinionPageViewModel : ViewModelBase
    {
        private EncuestaViewModel _encuestas;

        private readonly INavigationService _navigationService;
        private readonly IApiService _apiService;
        private readonly IDialogService _dialogService;



        private bool _visibleNoEncuesta;
        public bool VisibleNoEncuesta
        {
            get => _visibleNoEncuesta;
            set => SetProperty(ref _visibleNoEncuesta, value);
        }


        private bool _visibleCarousel;
        public bool VisibleCarousel
        {
            get => _visibleCarousel;
            set => SetProperty(ref _visibleCarousel, value);
        }


        private bool _isRunning;
        private bool _isVisible;
        public bool IsRunning
        {
            get => _isRunning;
            set => SetProperty(ref _isRunning, value);
        }

        public bool IsVisible
        {
            get => _isVisible;
            set => SetProperty(ref _isVisible, value);
        }

        public EncuestaViewModel Encuestas
        {
            get { return _encuestas; }
            set => SetProperty(ref _encuestas, value);
        } 


        public IPageDialogService PageDialogService { get; }
        public IPopupService PopupService { get; }
                       
        public OpinionPageViewModel(IPopupService popupService, IPageDialogService pageDialogService, INavigationService navigationService, IEventAggregator eventAggregator, IApiService apiService, IDialogService dialogService) : base(navigationService, eventAggregator)
        {
            PageDialogService = pageDialogService;
            PopupService = popupService;
            IsFabButtonVisible = false;

            _navigationService = navigationService;
            _apiService = apiService;
            _dialogService = dialogService;            
    
            IsRunning = true;
            IsVisible = false;  
        }        

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);
            var navigationMode = parameters.GetNavigationMode();
            if (navigationMode == NavigationMode.New)
            {
                LoadData();
            }
        }
               
        private async void LoadData()
        {

            IsRunning = true;  
            IsVisible = false; 
            try
            {

                var Cliente = JsonConvert.DeserializeObject<Cliente>(Settings.User);
                var Controller = "/Encuestas?CodCentroCosto=&CodCliente="+Cliente.CodCliente;

                var url = App.Current.Resources["UrlAPI"].ToString();
                var _app = App.Current.Resources["UrlApp"].ToString();
                var response = await _apiService.GetEncuesta(url, _app, Controller);
                if (!response.IsSuccess)
                {
                    IsRunning = false;
                    CustomDialog.ShowAlert(_dialogService, "Error", response.Message.ToString());
                    VisibleNoEncuesta = true;
                    VisibleCarousel = false;
                    return;
                }

                VisibleNoEncuesta = false;
                VisibleCarousel = true;  

                var _dataLista = response.Result;
                var _data = _dataLista[0];
              
                Encuestas = new EncuestaViewModel()
                {
                    IdEncuesta = _data.IdEncuesta,
                    Titulo = _data.Titulo,
                    Descripcion = _data.Descripcion,
                    CodCentroCosto = _data.CodCentroCosto,
                    Detalle = _data.Detalle.Select(x => new DetallaViewModel() {
                         Descripcion = x.Descripcion,
                         IdPregunta = x.IdPregunta,
                         Opiniones = x.Opiniones.Select( y=> new OpinionViewModel() {
                                 IdRespuesta = y.IdRespuesta,
                                 IdTipoOpinion = y.IdTipoOpinion,
                                 TextoAbierto = y.TextoAbierto,
                                 Titulo = y.Titulo                                 
                                 }).ToList()
                    }).ToList()
                };
                Encuestas.EnumeracionPreguntas();  
 

                await Task.Delay(1000);
                IsRunning = false;
                IsVisible = true;
            }
            catch (Exception ex)
            {
                IsRunning = false;
                var msg = ex.Message;
                IsVisible = true;
            }
        }


        public  async void EnviarEncuestas()
        {
            var codigoCliente = (JsonConvert.DeserializeObject<Cliente>(Settings.User)).CodCliente;
            var request = Encuestas.Detalle.Select(x=> new EncuestaRequest() { 
                 CodCliente = codigoCliente,
                 IdEncuesta = Encuestas.IdEncuesta,
                 IdOpinion = (x.OpcionSeleccionda == null) ? x.Opiniones.FirstOrDefault().IdTipoOpinion : x.OpcionSeleccionda.IdTipoOpinion,
                 IdPregunta = x.IdPregunta,
                 IdRespuesta = (x.OpcionSeleccionda == null)? x.Opiniones.FirstOrDefault().IdRespuesta: x.OpcionSeleccionda.IdRespuesta,
                 TextoRespuesta = (x.TextoRespuesta == null)?"": x.TextoRespuesta
            }).ToList();
                                          

            var url = App.Current.Resources["UrlAPI"].ToString();
            var _app = App.Current.Resources["UrlApp"].ToString();
            var response = await _apiService.SendResultEncuesta(url, _app, "/EncuestasRegistro", request);

            //IsRunning = false;
            //IsEnable = true;

            if (!response.IsSuccess)
            {
                //CustomDialog.ShowAlert(_dialogService, "Error", response.Message.ToString());
                await App.Current.MainPage.DisplayAlert("Error", response.Message.ToString(), "Ok");
                return;
            }            

            await Task.Delay(1000);
        }
    }
}
