﻿using Coral.Prism.Helper;
using Coral.Prism.Models;
using Newtonsoft.Json;
using Prism.Commands;
using Prism.Events;
using Prism.Navigation;
using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Xamarin.Forms;

namespace Coral.Prism.ViewModels
{
    public class MasterDetailShellPageViewModel : ViewModelBase
    {
        private readonly INavigationService _navigationService;
        private DelegateCommand _cerrarSesionCommand;
        private DelegateCommand _editProfileCommand;
        private readonly IDialogService _dialogService;
        private static MasterDetailShellPageViewModel _instance;

        private ImageSource _foto;
        private string _nombre;

        public DelegateCommand CerrarSesionCommand => _cerrarSesionCommand ?? (_cerrarSesionCommand = new DelegateCommand(ExecuteCerrarSesionCommand));

        public MasterDetailShellPageViewModel(INavigationService navigationService, IEventAggregator eventAggregator, IDialogService dialogService) : base(navigationService, eventAggregator)
        {
            _instance = this;
            _navigationService = navigationService;
            _dialogService = dialogService;
            LoadMenus();
            SetProfile();
        }


        //EL METODO QUE SE EJECUTA LLAMA CON EL CLICK
        async void ExecuteCerrarSesionCommand()
        {
            Settings.IsLogin = false;
            Settings.User = string.Empty;
            Settings.Rol = 0;
            await _navigationService.NavigateAsync("/LoginPage");
        }


        public ImageSource Foto
        {
            get => _foto;
            set => SetProperty(ref _foto, value);
        }

        public string Nombre
        {
            get => _nombre;
            set => SetProperty(ref _nombre, value);
        }

        public static MasterDetailShellPageViewModel GetInstance()
        {
            return _instance;
        }

        public ObservableCollection<MenuItemViewModel> Menus { get; set; }

        public DelegateCommand EditProfileCommand => _editProfileCommand ?? (_editProfileCommand = new DelegateCommand(ExecuteEditProfileCommand));


        private void LoadMenus()
        {
            List<Coral.Prism.Models.Menu> menus;
            if (Settings.Rol.Equals(1))
            {
                menus = new List<Coral.Prism.Models.Menu>
            {
                new Coral.Prism.Models.Menu
                {
                    Icon = "menu.png",
                    PageName = "MainPage",
                    Title = "Menú",
                    Index = 0
                },
                new Coral.Prism.Models.Menu
                {
                    Icon = "encuesta.png",
                    PageName = "MainPage",
                    Title = "Encuesta",
                    Index = 1
                },
                new Coral.Prism.Models.Menu
                {
                    Icon = "eventos.png",
                    PageName = "MainPage",
                    Title = "Eventos",
                    Index = 2
                },
                new Coral.Prism.Models.Menu
                {
                    Icon = "noticias.png",
                    PageName = "NoticiasPage",
                    Title = "Noticias",
                    Index = 3
                },
            };
            }
            else
            {
                menus = new List<Coral.Prism.Models.Menu>
                {
                    new Coral.Prism.Models.Menu
                    {
                        Icon = "menu.png",
                        PageName = "MainPage",
                        Title = "Menú",
                        Index = 0
                    },
                    new Coral.Prism.Models.Menu
                    {
                        Icon = "encuesta.png",
                        PageName = "MainPage",
                        Title = "Encuesta",
                        Index = 1
                    },
                    new Coral.Prism.Models.Menu
                    {
                        Icon = "noticias.png",
                        PageName = "NoticiasPage",
                        Title = "Noticias",
                        Index = 3
                    },
                };
            }

            Menus = new ObservableCollection<MenuItemViewModel>(
                menus.Select(m => new MenuItemViewModel(_navigationService)
                {
                    Icon = m.Icon,
                    PageName = m.PageName,
                    Title = m.Title,
                    Index = m.Index
                }).ToList());
        }

        async void ExecuteEditProfileCommand()
        {
            await _navigationService.NavigateAsync("ProfilePage", useModalNavigation: true);
        }


        public void SetProfile()
        {
            var user = JsonConvert.DeserializeObject<Cliente>(Settings.User);
            Foto = new UriImageSource
            {
                Uri = new Uri(user.Foto),
                CachingEnabled = false,
                CacheValidity = new TimeSpan(1, 0, 0, 0)
            };
            Nombre = user.Nombre;
        }


    }
}
