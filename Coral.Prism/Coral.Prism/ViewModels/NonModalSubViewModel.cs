﻿using Prism.Events;
using Prism.Navigation;

namespace Coral.Prism.ViewModels
{
    public class NonModalSubViewModel : ViewModelBase
    {
        public NonModalSubViewModel(INavigationService navigationService, IEventAggregator eventAggregator)
            : base(navigationService, eventAggregator)
        {
        }
    }
}
