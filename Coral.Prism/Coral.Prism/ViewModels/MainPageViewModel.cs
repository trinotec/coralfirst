﻿using Coral.Prism.Helper;
using Coral.Prism.Models;
using Coral.Prism.Services.Interfaces;
using Newtonsoft.Json;
using Prism.Commands;
using Prism.Events;
using Prism.Navigation;
using Prism.Services;
using Syncfusion.SfCarousel.XForms;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
 

namespace Coral.Prism.ViewModels
{
    public class MainPageViewModel : ViewModelBase, INavigationAware
    {
        private readonly IApiService _apiService;
        public IPageDialogService PageDialogService { get; }
        public IPopupService PopupService { get; }        

        private ObservableCollection<MenuItem> _menus;
        public ObservableCollection<MenuItem> Menus
        {
            get => _menus;
            set => SetProperty(ref _menus, value);
        }

        //private ObservableCollection<PlatilloItemViewModel> _platillosDia;
        //public ObservableCollection<PlatilloItemViewModel> PlatillosDia 
        //{
        //    get => _platillosDia;
        //    set => SetProperty(ref _platillosDia, value);
        //}

        //private ObservableCollection<PlatilloItemViewModel> _platillosSemana;
        //public ObservableCollection<PlatilloItemViewModel> PlatillosSemana
        //{
        //    get => _platillosSemana;
        //    set => SetProperty(ref _platillosSemana, value);
        //}

        //private ObservableCollection<ControlVistaPlatillos> _vistaCarouselMenu;
        //public ObservableCollection<ControlVistaPlatillos> VistaCarouselMenu
        //{
        //    get => _vistaCarouselMenu;
        //    set => SetProperty(ref _vistaCarouselMenu, value);
        //}

        //private MenuComidaResponse _menuDias;
        //public MenuComidaResponse MenuDias
        //{
        //    get => _menuDias;
        //    set => SetProperty(ref _menuDias, value);
        //}
        public List<PlatilloGroup> Dia1 { get; private set; } = new List<PlatilloGroup>();
        public List<PlatilloGroup> Dia2 { get; private set; } = new List<PlatilloGroup>();
        public List<PlatilloGroup> Dia3 { get; private set; } = new List<PlatilloGroup>();
        public List<PlatilloGroup> Dia4 { get; private set; } = new List<PlatilloGroup>();
        public List<PlatilloGroup> Dia5 { get; private set; } = new List<PlatilloGroup>();
        public List<PlatilloGroup> Dia6 { get; private set; } = new List<PlatilloGroup>();
        public List<PlatilloGroup> Dia7 { get; private set; } = new List<PlatilloGroup>();

        private bool _visibleD1;
        public bool VisibleD1
        {
            get => _visibleD1;
            set => SetProperty(ref _visibleD1, value);
        }

        private bool _visibleD2;
        public bool VisibleD2
        {
            get => _visibleD2;
            set => SetProperty(ref _visibleD2, value);
        }

        private bool _visibleD3;
        public bool VisibleD3
        {
            get => _visibleD3;
            set => SetProperty(ref _visibleD3, value);
        }

        private bool _visibleD4;
        public bool VisibleD4
        {
            get => _visibleD4;
            set => SetProperty(ref _visibleD4, value);
        }

        private bool _visibleD5;
        public bool VisibleD5
        {
            get => _visibleD5;
            set => SetProperty(ref _visibleD5, value);
        }

        private bool _visibleD6;
        public bool VisibleD6
        {
            get => _visibleD6;
            set => SetProperty(ref _visibleD6, value);
        }

        private bool _visibleD7;
        public bool VisibleD7
        {
            get => _visibleD7;
            set => SetProperty(ref _visibleD7, value);
        }


        private bool _selected1; 
        public bool Selected1
        {
            get => _selected1;
            set => SetProperty(ref _selected1, value);
        }

        private bool _selected2;
        public bool Selected2
        {
            get => _selected2;
            set => SetProperty(ref _selected2, value);
        }

        private bool _selected3;
        public bool Selected3
        {
            get => _selected3;
            set => SetProperty(ref _selected3, value);
        }

        private bool _selected4;
        public bool Selected4
        {
            get => _selected4;
            set => SetProperty(ref _selected4, value);
        }

        private bool _isRunning;
        public bool IsRunning
        {
            get => _isRunning;
            set => SetProperty(ref _isRunning, value);
        }

        private bool _isVisible;
        public bool IsVisible
        {
            get => _isVisible;
            set => SetProperty(ref _isVisible, value);
        }

        private DelegateCommand _nonModalCommand;
        public DelegateCommand NonModalCommand => _nonModalCommand ?? (_nonModalCommand = new DelegateCommand(async () => await NavigationService.NavigateAsync("NonModalSubPage")));

        private DelegateCommand _modalCommand;
        public DelegateCommand ModalCommand => _modalCommand ?? (_modalCommand = new DelegateCommand(async () => await NavigationService.NavigateAsync("ModalSubPage", useModalNavigation: true)));

        private DelegateCommand _fabCommand;
        public DelegateCommand FabCommand => _fabCommand ?? (_fabCommand = new DelegateCommand(() => IsFabButtonVisible = !IsFabButtonVisible));

        private DelegateCommand _chatCommand;
        public DelegateCommand ChatCommand => _chatCommand ?? (_chatCommand = new DelegateCommand(async () => await PageDialogService.DisplayAlertAsync("Chat", "Look ma a chat!", "Ok")));

        private DelegateCommand _popupCommand;
        public DelegateCommand PopupCommand => _popupCommand ?? (_popupCommand = new DelegateCommand(() => PopupService.DisplayPopup("Popup", "Look our own popup", new DelegateCommand<Controls.PopupResultEventArgs>((result) =>
        {
            var t = result;
        }))));

        private DelegateCommand<SelectionChangedEventArgs> _selectMenuCommand;
        private readonly INavigationService _navigationService;

        public DelegateCommand<SelectionChangedEventArgs> SelectEvaluacionCommand => _selectMenuCommand ?? (_selectMenuCommand = new DelegateCommand<SelectionChangedEventArgs>(SelectMenu));

        private static MainPageViewModel _instance;



        #region ListView syncfusion
        //private ObservableCollection<EncuestaInfo> _encuestaInfo;
        //private EncuestaInfo _encuestaOpcion;


        //public EncuestaInfo EncuestaOpcion
        //{
        //    get
        //    {
        //        return _encuestaOpcion;
        //    }
        //    set
        //    {
        //        if (_encuestaOpcion != value)
        //        {
        //            _encuestaOpcion = value;                    
        //            //Aqui le puedo meter mas cosas :D
        //        }
        //    }
        //}



        //public ObservableCollection<EncuestaInfo> EncuestaInfo
        //{
        //    get { return _encuestaInfo; }
        //    set { this._encuestaInfo = value; }
        //}

        //public void BookInfoRepository()
        //{
        //    GenerateBookInfo();
        //}

        //internal void GenerateBookInfo()
        //{
        //    EncuestaInfo = new ObservableCollection<EncuestaInfo>();
        //    EncuestaInfo.Add(new EncuestaInfo() { Opcion = "Excelete", Color= Xamarin.Forms.Color.FromHex("#5f5fc4") });
        //    EncuestaInfo.Add(new EncuestaInfo() { Opcion = "Bueno", Color = Xamarin.Forms.Color.FromHex("#5f5fc4") });
        //    EncuestaInfo.Add(new EncuestaInfo() { Opcion = "Normal", Color = Xamarin.Forms.Color.FromHex("#5f5fc4") });
        //    EncuestaInfo.Add(new EncuestaInfo() { Opcion = "Malo", Color = Xamarin.Forms.Color.FromHex("#5f5fc4") });
        //}



        #endregion




        public MainPageViewModel(
        IPopupService popupService, 
        IPageDialogService pageDialogService, 
        INavigationService navigationService,
        IEventAggregator eventAggregator, IApiService apiService) : base(navigationService, eventAggregator)
        {
            
            PageDialogService = pageDialogService;
            this._navigationService = navigationService;
            _apiService = apiService;
            PopupService = popupService;
            IsFabButtonVisible = true;
            _instance = this;
            SelectedMenu = 0;
            Selected1 = true;
            LoadData();
            
        }

        public static MainPageViewModel GetInstance()
        {
            return _instance;
        }

        private async void LoadData()
        {
            #region MenuLateral
            IsRunning = true;
            IsVisible = false;
            List<MenuItem >menus;
            if (Settings.Rol.Equals(1))
            {
                menus = new List<MenuItem>
            {
                new MenuItem
                {
                    Image = "bandeja.png",
                    SubTitulo = "Menú",
                    Titulo = "Menú"
                },
                new MenuItem
                {
                    Image = "revision.png",
                    SubTitulo = "Tu Opinión",
                    Titulo = "Encuesta"
                },
                new MenuItem
                {
                    Image = "calendario.png",
                    SubTitulo = "Reservación",
                    Titulo = "Eventos"
                },
                new MenuItem
                {
                    Image = "tableta.png",
                    SubTitulo = "Nuestras",
                    Titulo = "Noticias"
                },
            };
            }
            else
            {
                menus = new List<MenuItem>
                {
                    new MenuItem
                    {
                        Image = "bandeja.png",
                        SubTitulo = "Menú",
                        Titulo = "Menú"
                    },
                    new MenuItem
                    {
                        Image = "revision.png",
                        SubTitulo = "Tu Opinión",
                        Titulo = "Encuesta"
                    },
                    new MenuItem
                    {
                        Image = "tableta.png",
                        SubTitulo = "Nuestras",
                        Titulo = "Noticias"
                    },
                };
            }

            Menus = new ObservableCollection<MenuItem>(
                menus.Select(m => new MenuItem
                {
                    Image = m.Image,
                    SubTitulo = m.SubTitulo,
                    Titulo = m.Titulo,
                    
                    
                }).ToList());
            #endregion

            try
            {
                var url = App.Current.Resources["UrlAPI"].ToString(); 
                var _app = App.Current.Resources["UrlApp"].ToString();
                 var response = await _apiService.GetMenuByCategoria(url, _app, "/Banners/MenuSemanal");
                if (!response.IsSuccess)
                {
                    IsRunning = false;
                    await App.Current.MainPage.DisplayAlert("Error", response.Message.ToString(), "Ok");
                    //CustomDialog.ShowAlert(_dialogService, "Error", response.Message.ToString());
                    return;
                }

                var data = response.Result;
                data.Dia1.ToList().ForEach(f => {
                    Dia1.Add(new PlatilloGroup(f.Tipos.Descripcion, f.Tipos.Platilos.ToList()));
                });
                VisibleD1 = Dia1.Count > 0;
                data.Dia2.ToList().ForEach(f => {
                    Dia2.Add(new PlatilloGroup(f.Tipos.Descripcion, f.Tipos.Platilos.ToList()));
                });
                VisibleD2 = Dia2.Count > 0;
                data.Dia3.ToList().ForEach(f => {
                    Dia3.Add(new PlatilloGroup(f.Tipos.Descripcion, f.Tipos.Platilos.ToList()));
                });
                VisibleD3 = Dia3.Count > 0;
                data.Dia4.ToList().ForEach(f => {
                    Dia4.Add(new PlatilloGroup(f.Tipos.Descripcion, f.Tipos.Platilos.ToList()));
                });
                VisibleD4 = Dia4.Count > 0;
                data.Dia5.ToList().ForEach(f => {
                    Dia5.Add(new PlatilloGroup(f.Tipos.Descripcion, f.Tipos.Platilos.ToList()));
                });
                VisibleD5 = Dia5.Count > 0;
                data.Dia6.ToList().ForEach(f =>
                {
                    Dia6.Add(new PlatilloGroup(f.Tipos.Descripcion, f.Tipos.Platilos.ToList()));
                });
                VisibleD6 = Dia6.Count > 0;
                data.Dia7.ToList().ForEach(f => {
                    Dia7.Add(new PlatilloGroup(f.Tipos.Descripcion, f.Tipos.Platilos.ToList()));
                });
                VisibleD7 = Dia7.Count > 0;
                //PlatillosDia = new ObservableCollection<PlatilloItemViewModel>(
                //    _data.Dia.Select(
                //        s => new PlatilloItemViewModel(_navigationService)
                //        {
                //            CodBanner = s.CodBanner,
                //            url = s.url,
                //            Activo = s.Activo,
                //            ImagenMenu = s.ImagenMenu,
                //            Nombre = s.Nombre.Trim(),
                //            Tipo = s.Tipo,
                //            Calorias = s.Calorias.Trim()
                //        })
                //    .ToList());

                //PlatillosSemana = new ObservableCollection<PlatilloItemViewModel>(
                //    _data.Semana.Select(
                //        s => new PlatilloItemViewModel(_navigationService)
                //        {
                //            CodBanner = s.CodBanner,
                //            url = s.url,
                //            Activo = s.Activo,
                //            ImagenMenu = s.ImagenMenu,
                //            Nombre = s.Nombre.Trim(),
                //            Tipo = s.Tipo,
                //            Calorias = s.Calorias.Trim()
                //        })
                //    .ToList());



                //VistaCarouselMenu = new ObservableCollection<ControlVistaPlatillos>
                //{
                //    new ControlVistaPlatillos { Titulo = "DISHES OF THE DAY", Nota = string.Empty, Platilo = PlatillosDia },
                //    new ControlVistaPlatillos { Titulo = "FITTINGS", Nota = "*Todos los platos vienen con la ensalada del día", Platilo = PlatillosSemana }
                //};

                IsRunning = false;
                IsVisible = true;

                
            }
            catch (Exception ex)
            {
                IsRunning = false;
                var msg = ex.Message;
                IsVisible = true;
            }
        }

        private void SelectMenu(SelectionChangedEventArgs obj)
        {
            int selectedIndex = obj.SelectedIndex;
            UpdateTabMenu(selectedIndex);
            /*
            int selectedIndex = obj.SelectedIndex;

            switch (selectedIndex)
            {
                case 0:
                    Selected1 = true;
                    Selected2 = false;
                    Selected3 = false;
                    Selected4 = false;
                    break;
                case 1:
                    Selected1 = false;
                    Selected2 = true;
                    Selected3 = false;
                    Selected4 = false;
                    //await NavigationService.NavigateAsync("OpinionPage");
                    break;
                case 2:
                    Selected1 = false;
                    Selected2 = false;
                    Selected3 = true;
                    Selected4 = false;
                    //await NavigationService.NavigateAsync("EventosPage");
                    break;
                case 3:
                    Selected1 = true;
                    Selected2 = false;
                    Selected3 = false;
                    Selected4 = false;
                    await NavigationService.NavigateAsync("NoticiasPage");
                    break;

            } */
        }

        public async void UpdateTabMenu(int Index)
        {
            ((Xamarin.Forms.MasterDetailPage)Xamarin.Forms.Application.Current.MainPage).IsPresented = false;
            if (Settings.Rol.Equals(1))
            {
                switch (Index)
                {
                    case 0:
                        Selected1 = true;
                        Selected2 = false;
                        Selected3 = false;
                        Selected4 = false;
                        SelectedMenu = 0;
                        break;
                    case 1:
                        Selected1 = false;
                        Selected2 = true;
                        Selected3 = false;
                        Selected4 = false;
                        SelectedMenu = 1;
                        //await NavigationService.NavigateAsync("OpinionPage");
                        break;
                    case 2:
                        Selected1 = false;
                        Selected2 = false;
                        Selected3 = true;
                        Selected4 = false;
                        SelectedMenu = 2;
                        //await NavigationService.NavigateAsync("EventosPage");
                        break;
                    case 3:
                        Selected1 = true;
                        Selected2 = false;
                        Selected3 = false;
                        Selected4 = false;
                        //var nav = _navigationService.GetNavigationUriPath();
                        //await NavigationService.NavigateAsync("/MasterDetailShellPage/NavigationPage/MainPage/NoticiasPage");
                        await NavigationService.NavigateAsync("NoticiasPage");
                        SelectedMenu = 0;
                        break;
                }
            }
            else
            {
                switch (Index)
                {
                    case 0:
                        Selected1 = true;
                        Selected2 = false;
                        Selected3 = false;
                        Selected4 = false;
                        SelectedMenu = 0;
                        break;
                    case 1:
                        Selected1 = false;
                        Selected2 = true;
                        Selected3 = false;
                        Selected4 = false;
                        SelectedMenu = 1;
                        //await NavigationService.NavigateAsync("OpinionPage");
                        break;
                    case 2:
                        Selected1 = true;
                        Selected2 = false;
                        Selected3 = false;
                        Selected4 = false;
                        //var nav = _navigationService.GetNavigationUriPath();
                        //await NavigationService.NavigateAsync("/MasterDetailShellPage/NavigationPage/MainPage/NoticiasPage");
                        await NavigationService.NavigateAsync("NoticiasPage");
                        SelectedMenu = 0;
                        break;
                }
            }
        }

        private async void LoadNoticias()
        {
            var url = App.Current.Resources["UrlAPI"].ToString();
            var _app = App.Current.Resources["UrlApp"].ToString();
            var response = await _apiService.GetListNoticias(url, _app, "/Noticias");
            if (!response.IsSuccess)
            {
                return;
            }
            Settings.News = JsonConvert.SerializeObject(response.Result);
        }
        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            //if( string.IsNullOrEmpty(parameters.GetValue<string>("Index")) )
            //{
            //    SelectedMenu = 0;
            //    UpdateTabMenu(SelectedMenu);
            //}
            //else
            //{
            //    SelectedMenu = Int32.Parse(parameters.GetValue<string>("Index"));
            //    UpdateTabMenu(SelectedMenu);
            //}

            if (string.IsNullOrEmpty(Settings.News))
            {
                LoadNoticias();
            }
        }

    }
     
}
