﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Coral.Prism.Models
{
    public class MenuItem
    {
        public string Image { get; set; }
        public string Titulo { get; set; }
        public string SubTitulo { get; set; }
    }
}
