﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Coral.Prism.Models
{
    public class Comida
    {
        public int CodBanner { get; set; }
        public string Nombre { get; set; }
        public string url { get; set; }
        public bool Activo { get; set; }
        public bool ImagenMenu { get; set; }
        public string Tipo { get; set; }
        public string Calorias { get; set; }

    }
}
