﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Coral.Prism.Models
{
    public class Encuesta
    {
        public int IdEncuesta { get; set; }
        public string Titulo { get; set; }
        public string Descripcion { get; set; }
        public string CodCentroCosto { get; set; }
        public List<Detalle> Detalle { get; set; }
    }
}
