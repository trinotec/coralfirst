﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Coral.Prism.Models
{
    public class Cliente
    {
        public string Email { get; set; }
        public string CodCliente { get; set; }
        public string Nombre { get; set; }
        public bool CambiarClave { get; set; }
        public bool NotificacionConsumoBajo { get; set; }
        public float MontoConsumoBajo { get; set; }
        public bool NotificacionCadaConsumo { get; set; }
        public string Telefono { get; set; }
        public string Genero { get; set; }
        public string FechaCumpleAnos { get; set; }
        public string Foto { get; set; }
        public int TipoRol { get; set; }

    }
}
