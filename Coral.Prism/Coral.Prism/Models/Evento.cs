﻿using Coral.Prism.Helper;
using System;
using System.Collections.Generic;
using System.Text;

namespace Coral.Prism.Models
{
    public class Evento
    {        
        public int idEvento { get; set; }
        public string CodCliente { get; set; }
        public object NomCliente { get; set; }
        public DateTime FechaIngreso { get; set; }
        public string NombreEvento { get; set; }
        public int CantPersonas { get; set; }
        public DateTime HoraInicio { get; set; }
        public DateTime HoraFinal { get; set; }
        public string DescripcionLarga { get; set; }

        public string MesCorto
        {
            get
            {                
                return HelpFormat.MesString(HoraInicio.Month);
            }
        }

        public string HorarioEvento
        {
            get
            {
                var HoraIni = HelpFormat.FormatoFechaDatetime(HoraInicio);
                var HoraF = HelpFormat.FormatoFechaDatetime(HoraFinal);
                return  $"{HoraIni} a {HoraF}";
                
            }
        }


    }
}
