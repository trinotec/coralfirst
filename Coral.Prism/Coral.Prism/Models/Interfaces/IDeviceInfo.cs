﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Coral.Prism.Models.Interfaces
{
    public interface IDeviceInfo
    {
        float StatusbarHeight { get; }
    }
}
