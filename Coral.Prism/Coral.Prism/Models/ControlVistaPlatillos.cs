﻿using Coral.Prism.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Coral.Prism.Models
{
    public class ControlVistaPlatillos
    {
        public string Nota { get; set; }
        public string Titulo { get; set; }
        public ObservableCollection<PlatilloItemViewModel> Platilo { get; set; }


    }
}
