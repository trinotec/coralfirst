﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Coral.Prism.Models
{
    public class TipoResponse
    {
        public string Tipo { get; set; }
        public string Descripcion { get; set; }
        public ICollection<Comida> Platilos { get; set; }
    }
}
