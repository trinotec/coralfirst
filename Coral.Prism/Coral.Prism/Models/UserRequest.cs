﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Coral.Prism.Models
{
    public class UserRequest : Cliente
    {
        public string ImagenBase64 { get; set; }
        public bool Activo { get; set; }
    }
}
