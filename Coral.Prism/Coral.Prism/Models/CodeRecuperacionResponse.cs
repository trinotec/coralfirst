﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Coral.Prism.Models
{
    public class CodeRecuperacionResponse
    {
        public bool Success { get; set; }
        public string CodigoRecuperacion { get; set; }
    }
}
