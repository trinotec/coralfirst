﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Coral.Prism.Models
{
    public class Detalle
    {
        public string Descripcion { get; set; }
        public int IdPregunta { get; set; }
        public List<Opinione> Opiniones { get; set; }
    }
}
