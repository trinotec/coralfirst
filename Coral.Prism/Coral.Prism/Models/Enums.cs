﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Coral.Prism.Models
{
    public enum PageMode
    {
        None,
        Menu,
        Navigate,
        Modal
    }
}
