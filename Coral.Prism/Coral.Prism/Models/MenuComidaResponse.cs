﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Coral.Prism.Models
{
    public class MenuComidaResponse
    {
        public ICollection<DiaResponse> Dia1 { get; set; }
        public ICollection<DiaResponse> Dia2 { get; set; }
        public ICollection<DiaResponse> Dia3 { get; set; }
        public ICollection<DiaResponse> Dia4 { get; set; }
        public ICollection<DiaResponse> Dia5 { get; set; }
        public ICollection<DiaResponse> Dia6 { get; set; }
        public ICollection<DiaResponse> Dia7 { get; set; }
    }
}
