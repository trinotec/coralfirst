﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Coral.Prism.Models
{
    public class LoginRequest
    {
        public string codigo { get; set; }
        public string pw { get; set; }
    }
}
