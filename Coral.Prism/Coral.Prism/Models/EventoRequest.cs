﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Coral.Prism.Models
{
    public class EventoRequest
    {
        public int idEvento { get; set; }
        public string CodCliente { get; set; }
        public string NombreEvento { get; set; }
        public int CantPersonas { get; set; }
        public DateTime HoraInicio { get; set; }
        public DateTime HoraFinal { get; set; }
        public string DescripcionLarga { get; set; }
    }
}
