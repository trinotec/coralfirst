﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Coral.Prism.Models
{
    public class EncuestaRequest
    {
        public string CodCliente { get; set; }
        public int IdEncuesta { get; set; }
        public int IdOpinion { get; set; }
        public int IdPregunta { get; set; }
        public int IdRespuesta { get; set; }
        public string TextoRespuesta { get; set; } 

    }
}
