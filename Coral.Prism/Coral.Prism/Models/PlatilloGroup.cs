﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Coral.Prism.Models
{
    public class PlatilloGroup : List<Comida>
    {
        public string Name { get; private set; }

        public PlatilloGroup(string name, List<Comida> platillos) : base(platillos)
        {
            Name = name;
        }
    }
}
