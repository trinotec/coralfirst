﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Coral.Prism.Models
{
    public class UserResponse
    {
        public bool Success { get; set; }
        public string Nombre { get; set; }
        public string Imagen { get; set; }
    }
}
