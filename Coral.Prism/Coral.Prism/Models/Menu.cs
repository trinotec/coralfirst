﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Coral.Prism.Models
{
    public class Menu
    {
        public string Icon { get; set; }
        public string Title { get; set; }
        public string PageName { get; set; }
        public string Navegacion { get; set; }
        public int Index { get; set; }
    }
}
