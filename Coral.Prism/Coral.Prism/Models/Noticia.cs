﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Coral.Prism.Models
{
    public class Noticia
    {
        public int IdNoticia { get; set; }
        public string Titulo { get; set; }
        public string url { get; set; }
        public string Autor { get; set; }
        public string Detalle { get; set; }
        public int Tipo { get; set; }
        public DateTime Fecha { get; set; }
        public DateTime FechaPublicacion { get; set; }
        public DateTime FechaCreacion { get; set; }
    }
}
