﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Coral.Prism.Models
{
    public class NoticiasResponse
    {
        public ICollection<Noticia> NoticiasMasRecientes { get; set; }
    }
}
