﻿using FFImageLoading.Forms.Platform;
using Foundation;
using Prism;
using Prism.Ioc;
using Syncfusion.ListView.XForms.iOS;
using Syncfusion.SfCalendar.XForms.iOS;
using Syncfusion.SfCarousel.XForms.iOS;
using Syncfusion.XForms.iOS.Border;
using Syncfusion.XForms.iOS.Buttons;
using Syncfusion.XForms.iOS.Shimmer;
using Syncfusion.XForms.iOS.TabView;
using Syncfusion.XForms.iOS.TextInputLayout;
using Syncfusion.XForms.Pickers.iOS;
using System;
using System.Linq;
using UIKit;
using Xamarin.Forms;

namespace Coral.Prism.iOS
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the 
    // User Interface of the application, as well as listening (and optionally responding) to 
    // application events from iOS.
    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {
        //
        // This method is invoked when the application has loaded and is ready to run. In this 
        // method you should instantiate the window, load the UI into it and then make the window
        // visible.
        //
        // You have 17 seconds to return from this method, or iOS will terminate your application.
        //
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            Forms.SetFlags(new string[] { "CarouselView_Experimental", "IndicatorView_Experimental" });
            global::Xamarin.Forms.Forms.Init();
            new SfCarouselRenderer();
            SfTextInputLayoutRenderer.Init();
            SfTabViewRenderer.Init();
            SfListViewRenderer.Init();
            SfRadioButtonRenderer.Init();
            CachedImageRenderer.Init();
            SfShimmerRenderer.Init();
            SfBorderRenderer.Init();
            SfCalendarRenderer.Init();
            SfTimePickerRenderer.Init();

            foreach (var familyNames in UIFont.FamilyNames.OrderBy(c=>c).ToList())
            {
                Console.WriteLine(value: " * " + familyNames);
                foreach(var familyName in UIFont.FontNamesForFamilyName(familyNames).OrderBy(c => c).ToList())
                {
                    Console.WriteLine(value: "* -- " + familyName);
                }
            }

            LoadApplication(new App(new iOSInitializer()));

            return base.FinishedLaunching(app, options);
        }
    }

    public class iOSInitializer : IPlatformInitializer
    {
        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
            // Register any platform specific implementations
        }
    }
}
