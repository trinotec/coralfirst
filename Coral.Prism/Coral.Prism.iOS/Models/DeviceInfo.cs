﻿using Coral.Prism.iOS.Models;
using Coral.Prism.Models.Interfaces;
using UIKit;
using Xamarin.Forms;

[assembly: Dependency(typeof(DeviceInfo))]
namespace Coral.Prism.iOS.Models
{
    public class DeviceInfo : IDeviceInfo
    {
        public float StatusbarHeight => (float)UIApplication.SharedApplication.StatusBarFrame.Size.Height;
    }
}